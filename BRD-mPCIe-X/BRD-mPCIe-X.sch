<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="3" fill="8" visible="no" active="no"/>
<layer number="3" name="Route3" color="5" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="6" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="14" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="6" fill="7" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="7" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="3" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="7" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="3" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="3" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="3" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="15" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="15" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="12" fill="12" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="13" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="13" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="13" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="bCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Bemassung" color="7" fill="1" visible="no" active="yes"/>
<layer number="102" name="descript1" color="3" fill="1" visible="no" active="yes"/>
<layer number="103" name="HV_Net" color="12" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="no" active="no"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="no" active="no"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="no" active="no"/>
<layer number="108" name="Measures1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="Measures2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="CB1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="DAQ127,DAQ128,DAQ2543" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="dio1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="TermoCouple" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="DAC1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="OPTO1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="RELAY" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="tTestdril" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="bTestdril" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="tHOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="bHOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="tNeutral" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bNeutral" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="tPE" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="bPE" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="t+300V" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="b+300V" color="7" fill="1" visible="yes" active="yes"/>
<layer number="138" name="t-300V" color="7" fill="1" visible="yes" active="yes"/>
<layer number="139" name="b-300V" color="7" fill="1" visible="yes" active="yes"/>
<layer number="140" name="t+150V" color="7" fill="1" visible="yes" active="yes"/>
<layer number="141" name="b+150V" color="7" fill="1" visible="yes" active="yes"/>
<layer number="142" name="tLow" color="7" fill="1" visible="yes" active="yes"/>
<layer number="143" name="bLow" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="tCommon" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="bCommon" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="222" name="HIDDEN" color="14" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="252" name="MMARM-E" color="7" fill="1" visible="yes" active="yes"/>
<layer number="253" name="MM51C2/E" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames">
<packages>
</packages>
<symbols>
<symbol name="DINA4_L">
<wire x1="264.16" y1="0" x2="264.16" y2="180.34" width="0.4064" layer="94"/>
<wire x1="264.16" y1="180.34" x2="0" y2="180.34" width="0.4064" layer="94"/>
<wire x1="0" y1="180.34" x2="0" y2="0" width="0.4064" layer="94"/>
<wire x1="0" y1="0" x2="264.16" y2="0" width="0.4064" layer="94"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.254" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94" font="vector">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94" font="vector">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94" font="vector">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94" font="vector">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA4_L" prefix="FRAME">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with extra doc field</description>
<gates>
<gate name="G$1" symbol="DINA4_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="162.56" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="V">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="01PORT">
<packages>
</packages>
<symbols>
<symbol name="PORT5">
<wire x1="1.27" y1="0" x2="0" y2="1.27" width="0.2032" layer="95"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.2032" layer="95"/>
<wire x1="0" y1="1.27" x2="-9.8425" y2="1.27" width="0.2032" layer="95"/>
<wire x1="-9.8425" y1="1.27" x2="-9.8425" y2="-1.27" width="0.2032" layer="95"/>
<wire x1="-9.8425" y1="-1.27" x2="0" y2="-1.27" width="0.2032" layer="95"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PORT5" prefix="PORT">
<description>&lt;b&gt;PORT SYMBOL&lt;/b&gt;&lt;p&gt;
 for net name (5 characters)</description>
<gates>
<gate name="G$1" symbol="PORT5" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="02_supply2">
<packages>
</packages>
<symbols>
<symbol name="3.3V">
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<pin name="3.3V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<text x="-3.175" y="3.175" size="1.4224" layer="96" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="4.0V">
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<pin name="4.0V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="3.175" size="1.4224" layer="96" font="vector" align="bottom-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="3.3V">
<gates>
<gate name="G$1" symbol="3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="4.0V">
<gates>
<gate name="G$1" symbol="4.0V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="C0603C104K5RACTU">
<packages>
<package name="C0603C104K5RACTU">
<wire x1="-1.5812" y1="0.7938" x2="1.5811" y2="0.7938" width="0.4064" layer="21"/>
<wire x1="1.5812" y1="-0.7938" x2="-1.5812" y2="-0.7938" width="0.4064" layer="21"/>
<wire x1="-1.5875" y1="0.7875" x2="-1.5875" y2="-0.7874" width="0.4064" layer="21"/>
<wire x1="1.5875" y1="0.7875" x2="1.5875" y2="-0.7874" width="0.4064" layer="21"/>
<smd name="1" x="-0.7874" y="0" dx="0.762" dy="0.9652" layer="1"/>
<smd name="2" x="0.7874" y="0" dx="0.762" dy="0.9652" layer="1"/>
<text x="-2.413" y="-2.8575" size="1.27" layer="27" font="vector" ratio="18">&gt;VALUE</text>
<text x="-2.413" y="2.2225" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="C">
<text x="-3.175" y="4.445" size="1.4224" layer="95" font="vector">&gt;NAME</text>
<text x="-3.81" y="2.286" size="1.4224" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-1.524" x2="-0.508" y2="1.524" layer="94"/>
<rectangle x1="0.508" y1="-1.524" x2="1.27" y2="1.524" layer="94"/>
<pin name="1" x="-3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C0603C104K5RACTU" prefix="C">
<description>NAME:C0603C104K5RACTU;DESC:CAP CERAMIC .100UF 50V X7R 0603 -55+125C RoHS;SUP:Digi-Key 399-5089-1-ND;VER:004</description>
<gates>
<gate name="1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C0603C104K5RACTU">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="TPSC107K010R0100">
<packages>
<package name="TPSC107K010R0100">
<wire x1="-2.8" y1="1.55" x2="2.8" y2="1.55" width="0.4064" layer="21"/>
<wire x1="2.8" y1="-1.3075" x2="2.8" y2="-1.55" width="0.4064" layer="21"/>
<wire x1="2.8" y1="-1.55" x2="-2.8" y2="-1.55" width="0.4064" layer="21"/>
<wire x1="-2.8" y1="-1.55" x2="-2.8" y2="-1.3076" width="0.4064" layer="21"/>
<wire x1="2.8" y1="1.5501" x2="2.8" y2="1.3076" width="0.4064" layer="21"/>
<wire x1="-2.8" y1="1.3076" x2="-2.8" y2="1.55" width="0.4064" layer="21"/>
<wire x1="3.5065" y1="-1.5811" x2="4.144" y2="-1.5811" width="0.4064" layer="21"/>
<wire x1="3.8252" y1="-1.8999" x2="3.8252" y2="-1.2624" width="0.4064" layer="21"/>
<smd name="+" x="2.625" y="0" dx="2.75" dy="1.8" layer="1"/>
<smd name="-" x="-2.625" y="0" dx="2.75" dy="1.8" layer="1"/>
<text x="-2.8575" y="3.81" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
<text x="-3.4925" y="2.2225" size="1.27" layer="27" font="vector" ratio="18">&gt;VALUE</text>
<text x="2.3019" y="-0.7144" size="1.4224" layer="51" font="vector" ratio="18">+</text>
</package>
</packages>
<symbols>
<symbol name="CE">
<rectangle x1="-1.651" y1="-1.27" x2="1.651" y2="-0.381" layer="94"/>
<wire x1="-1.524" y1="0.381" x2="1.524" y2="0.381" width="0.254" layer="94"/>
<wire x1="1.524" y1="0.381" x2="1.524" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.524" y1="1.27" x2="-1.524" y2="0.381" width="0.254" layer="94"/>
<wire x1="-1.524" y1="1.27" x2="1.524" y2="1.27" width="0.254" layer="94"/>
<pin name="+" x="0" y="3.81" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="-" x="0" y="-3.81" visible="off" length="short" direction="pas" rot="R90"/>
<text x="2.5717" y="-0.3111" size="1.4224" layer="95" font="vector">&gt;NAME</text>
<text x="-0.5842" y="1.6764" size="1.27" layer="94" rot="R90">+</text>
<text x="2.5717" y="-2.2161" size="1.4224" layer="96" font="vector">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TPSC107K010R0100" prefix="C">
<description>NAME:TPSC107K010R0100;DESC:CAP TANT 100UF 10V 10% 2312 -55C +125C RoHS;SUP:Digikey 478-1765-1-ND;VER:004</description>
<gates>
<gate name="G$1" symbol="CE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TPSC107K010R0100">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="FM">
<packages>
<package name="FM">
<smd name="P$1" x="0" y="0" dx="1.016" dy="1.016" layer="1" roundness="100"/>
</package>
</packages>
<symbols>
<symbol name="FIDUCIAL_MARK">
</symbol>
</symbols>
<devicesets>
<deviceset name="DO_NOT_INSTALL_FM" prefix="FM">
<description>NAME:FIDUCIAL MARK;DESC:DO NOT INSTALL FIDUCIAL MARK 0.04";SUP:UNKNOWN;VER:003</description>
<gates>
<gate name="G$1" symbol="FIDUCIAL_MARK" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="NL-SW-LTE-S7588">
<packages>
<package name="NL-SW-LTE-S7588_S">
<description>Socket Part Number -  NPPN101BFLC-RC</description>
<circle x="14.9098" y="12.5476" radius="0.8728875" width="0" layer="21"/>
<circle x="14.83" y="3.58" radius="1.2" width="0.4064" layer="37"/>
<circle x="14.83" y="-3.66" radius="1.2" width="0.4064" layer="37"/>
<wire x1="10.45" y1="12.45" x2="10.45" y2="9.55" width="0.4064" layer="21"/>
<wire x1="-10.45" y1="9.55" x2="-10.45" y2="12.45" width="0.4064" layer="21"/>
<wire x1="17.0688" y1="14.7066" x2="17.0688" y2="-14.7066" width="0.4064" layer="21"/>
<wire x1="17.0688" y1="-14.7066" x2="-16.9672" y2="-14.7066" width="0.4064" layer="21"/>
<wire x1="-16.9672" y1="-14.7066" x2="-16.9672" y2="14.7066" width="0.4064" layer="21"/>
<wire x1="-16.9672" y1="14.7066" x2="17.0688" y2="14.7066" width="0.4064" layer="21"/>
<wire x1="9.963" y1="8.6614" x2="10.2932" y2="8.9916" width="0.2286" layer="21"/>
<wire x1="10.2932" y1="8.9916" x2="10.2932" y2="7.9502" width="0.2286" layer="21"/>
<wire x1="10.2932" y1="7.9502" x2="9.9376" y2="7.9502" width="0.2286" layer="21"/>
<wire x1="10.2932" y1="7.9502" x2="10.6488" y2="7.9502" width="0.2286" layer="21"/>
<wire x1="-10.0584" y1="8.6868" x2="-9.7282" y2="9.017" width="0.2286" layer="21"/>
<wire x1="-9.7282" y1="9.017" x2="-9.7282" y2="7.9756" width="0.2286" layer="21"/>
<wire x1="-9.7282" y1="7.9756" x2="-10.0838" y2="7.9756" width="0.2286" layer="21"/>
<wire x1="-9.7282" y1="7.9756" x2="-9.3726" y2="7.9756" width="0.2286" layer="21"/>
<wire x1="-8.9154" y1="8.8392" x2="-8.9154" y2="8.1534" width="0.2286" layer="21"/>
<wire x1="-8.9154" y1="8.1534" x2="-8.7376" y2="7.9756" width="0.2286" layer="21"/>
<wire x1="-8.7376" y1="7.9756" x2="-8.382" y2="7.9756" width="0.2286" layer="21"/>
<wire x1="-8.382" y1="7.9756" x2="-8.2042" y2="8.1534" width="0.2286" layer="21"/>
<wire x1="-8.2042" y1="8.1534" x2="-8.2042" y2="8.8392" width="0.2286" layer="21"/>
<wire x1="-8.2042" y1="8.8392" x2="-8.382" y2="9.017" width="0.2286" layer="21"/>
<wire x1="-8.382" y1="9.017" x2="-8.7376" y2="9.017" width="0.2286" layer="21"/>
<wire x1="-8.7376" y1="9.017" x2="-8.9154" y2="8.8392" width="0.2286" layer="21"/>
<wire x1="-8.2296" y1="8.8138" x2="-8.89" y2="8.1534" width="0.2286" layer="21"/>
<wire x1="-11.8806" y1="-8.2582" x2="-11.5504" y2="-7.928" width="0.2286" layer="21"/>
<wire x1="-11.5504" y1="-7.928" x2="-11.5504" y2="-8.9694" width="0.2286" layer="21"/>
<wire x1="-11.5504" y1="-8.9694" x2="-11.906" y2="-8.9694" width="0.2286" layer="21"/>
<wire x1="-11.5504" y1="-8.9694" x2="-11.1948" y2="-8.9694" width="0.2286" layer="21"/>
<wire x1="-10.6868" y1="-8.2582" x2="-10.3566" y2="-7.928" width="0.2286" layer="21"/>
<wire x1="-10.3566" y1="-7.928" x2="-10.3566" y2="-8.9694" width="0.2286" layer="21"/>
<wire x1="-10.3566" y1="-8.9694" x2="-10.7122" y2="-8.9694" width="0.2286" layer="21"/>
<wire x1="-10.3566" y1="-8.9694" x2="-10.001" y2="-8.9694" width="0.2286" layer="21"/>
<wire x1="8.4582" y1="-8.1534" x2="8.636" y2="-7.9756" width="0.2286" layer="21"/>
<wire x1="8.636" y1="-7.9756" x2="8.9662" y2="-7.9756" width="0.2286" layer="21"/>
<wire x1="8.9662" y1="-7.9756" x2="9.144" y2="-8.1534" width="0.2286" layer="21"/>
<wire x1="9.144" y1="-8.1534" x2="9.144" y2="-8.3312" width="0.2286" layer="21"/>
<wire x1="9.144" y1="-8.3312" x2="8.4582" y2="-9.017" width="0.2286" layer="21"/>
<wire x1="8.4582" y1="-9.017" x2="9.144" y2="-9.017" width="0.2286" layer="21"/>
<wire x1="9.6266" y1="-8.1534" x2="9.8044" y2="-7.9756" width="0.2286" layer="21"/>
<wire x1="9.8044" y1="-7.9756" x2="10.1346" y2="-7.9756" width="0.2286" layer="21"/>
<wire x1="10.1346" y1="-7.9756" x2="10.3124" y2="-8.1534" width="0.2286" layer="21"/>
<wire x1="10.3124" y1="-8.1534" x2="10.3124" y2="-8.8392" width="0.2286" layer="21"/>
<wire x1="10.3124" y1="-8.8392" x2="10.1346" y2="-9.017" width="0.2286" layer="21"/>
<wire x1="10.1346" y1="-9.017" x2="9.779" y2="-9.017" width="0.2286" layer="21"/>
<wire x1="9.779" y1="-9.017" x2="9.6266" y2="-8.8646" width="0.2286" layer="21"/>
<wire x1="9.6266" y1="-8.8646" x2="9.6266" y2="-8.1534" width="0.2286" layer="21"/>
<wire x1="9.6774" y1="-8.7884" x2="10.287" y2="-8.1788" width="0.2286" layer="21"/>
<wire x1="-10.45" y1="12.45" x2="-10.1" y2="12.45" width="0.4064" layer="21"/>
<wire x1="-10.45" y1="9.55" x2="-8.1" y2="9.55" width="0.4064" layer="21"/>
<wire x1="-7.95" y1="12.45" x2="-6.1" y2="12.45" width="0.4064" layer="21"/>
<wire x1="-3.95" y1="12.45" x2="-2.1" y2="12.45" width="0.4064" layer="21"/>
<wire x1="0.05" y1="12.45" x2="1.9" y2="12.45" width="0.4064" layer="21"/>
<wire x1="4.05" y1="12.45" x2="5.9" y2="12.45" width="0.4064" layer="21"/>
<wire x1="8.05" y1="12.45" x2="10.45" y2="12.45" width="0.4064" layer="21"/>
<wire x1="-5.95" y1="9.55" x2="-4.1" y2="9.55" width="0.4064" layer="21"/>
<wire x1="-1.95" y1="9.55" x2="-0.1" y2="9.55" width="0.4064" layer="21"/>
<wire x1="2.05" y1="9.55" x2="3.9" y2="9.55" width="0.4064" layer="21"/>
<wire x1="6.05" y1="9.55" x2="7.9" y2="9.55" width="0.4064" layer="21"/>
<wire x1="10.05" y1="9.55" x2="10.45" y2="9.55" width="0.4064" layer="21"/>
<wire x1="10.45" y1="-9.55" x2="10.45" y2="-12.45" width="0.4064" layer="21"/>
<wire x1="-10.45" y1="-12.45" x2="-10.45" y2="-9.55" width="0.4064" layer="21"/>
<wire x1="-10.45" y1="-9.55" x2="-10.1" y2="-9.55" width="0.4064" layer="21"/>
<wire x1="-10.45" y1="-12.45" x2="-8.1" y2="-12.45" width="0.4064" layer="21"/>
<wire x1="-7.95" y1="-9.55" x2="-6.1" y2="-9.55" width="0.4064" layer="21"/>
<wire x1="-3.95" y1="-9.55" x2="-2.1" y2="-9.55" width="0.4064" layer="21"/>
<wire x1="0.05" y1="-9.55" x2="1.9" y2="-9.55" width="0.4064" layer="21"/>
<wire x1="4.05" y1="-9.55" x2="5.9" y2="-9.55" width="0.4064" layer="21"/>
<wire x1="8.05" y1="-9.55" x2="10.45" y2="-9.55" width="0.4064" layer="21"/>
<wire x1="-5.95" y1="-12.45" x2="-4.1" y2="-12.45" width="0.4064" layer="21"/>
<wire x1="-1.95" y1="-12.45" x2="-0.1" y2="-12.45" width="0.4064" layer="21"/>
<wire x1="2.05" y1="-12.45" x2="3.9" y2="-12.45" width="0.4064" layer="21"/>
<wire x1="6.05" y1="-12.45" x2="7.9" y2="-12.45" width="0.4064" layer="21"/>
<wire x1="10.05" y1="-12.45" x2="10.45" y2="-12.45" width="0.4064" layer="21"/>
<smd name="1" x="9" y="9.605" dx="1.4" dy="2.29" layer="1"/>
<smd name="2" x="7" y="12.395" dx="1.4" dy="2.29" layer="1"/>
<smd name="3" x="5" y="9.605" dx="1.4" dy="2.29" layer="1"/>
<smd name="4" x="3" y="12.395" dx="1.4" dy="2.29" layer="1"/>
<smd name="5" x="1" y="9.605" dx="1.4" dy="2.29" layer="1"/>
<smd name="6" x="-1" y="12.395" dx="1.4" dy="2.29" layer="1"/>
<smd name="7" x="-3" y="9.605" dx="1.4" dy="2.29" layer="1"/>
<smd name="8" x="-5" y="12.395" dx="1.4" dy="2.29" layer="1"/>
<smd name="9" x="-7" y="9.605" dx="1.4" dy="2.29" layer="1"/>
<smd name="10" x="-9" y="12.395" dx="1.4" dy="2.29" layer="1"/>
<smd name="11" x="-9" y="-9.605" dx="1.4" dy="2.29" layer="1"/>
<smd name="12" x="-7" y="-12.395" dx="1.4" dy="2.29" layer="1"/>
<smd name="13" x="-5" y="-9.605" dx="1.4" dy="2.29" layer="1"/>
<smd name="14" x="-3" y="-12.395" dx="1.4" dy="2.29" layer="1"/>
<smd name="15" x="-1" y="-9.605" dx="1.4" dy="2.29" layer="1"/>
<smd name="16" x="1" y="-12.395" dx="1.4" dy="2.29" layer="1"/>
<smd name="17" x="3" y="-9.605" dx="1.4" dy="2.29" layer="1"/>
<smd name="18" x="5" y="-12.395" dx="1.4" dy="2.29" layer="1"/>
<smd name="19" x="7" y="-9.605" dx="1.4" dy="2.29" layer="1"/>
<smd name="20" x="9" y="-12.395" dx="1.4" dy="2.29" layer="1"/>
<text x="-2.8575" y="1.905" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
<text x="-3.4925" y="0.3175" size="1.27" layer="27" font="vector" ratio="18">&gt;VALUE</text>
</package>
<package name="NL-SW-LTE-S7588">
<description>Part Number: 950510-6102-AR Alternate P/N: NPPN101BFCN-RC</description>
<circle x="14.9098" y="12.5476" radius="0.8728875" width="0" layer="21"/>
<circle x="14.83" y="3.58" radius="1.2" width="0.4064" layer="37"/>
<circle x="14.83" y="-3.66" radius="1.2" width="0.4064" layer="37"/>
<wire x1="10.287" y1="12.2682" x2="10.287" y2="9.7282" width="0.4064" layer="21"/>
<wire x1="10.287" y1="9.7282" x2="-10.287" y2="9.7282" width="0.4064" layer="21"/>
<wire x1="-10.287" y1="9.7282" x2="-10.287" y2="12.2682" width="0.4064" layer="21"/>
<wire x1="-10.287" y1="12.2682" x2="10.287" y2="12.2682" width="0.4064" layer="21"/>
<wire x1="-10.287" y1="-9.7282" x2="10.287" y2="-9.7282" width="0.4064" layer="21"/>
<wire x1="10.287" y1="-9.7282" x2="10.287" y2="-12.2682" width="0.4064" layer="21"/>
<wire x1="10.287" y1="-12.2682" x2="-10.287" y2="-12.2682" width="0.4064" layer="21"/>
<wire x1="-10.287" y1="-12.2682" x2="-10.287" y2="-9.7282" width="0.4064" layer="21"/>
<wire x1="17.0688" y1="14.7066" x2="17.0688" y2="-14.7066" width="0.4064" layer="21"/>
<wire x1="17.0688" y1="-14.7066" x2="-16.9672" y2="-14.7066" width="0.4064" layer="21"/>
<wire x1="-16.9672" y1="-14.7066" x2="-16.9672" y2="14.7066" width="0.4064" layer="21"/>
<wire x1="-16.9672" y1="14.7066" x2="17.0688" y2="14.7066" width="0.4064" layer="21"/>
<wire x1="8.763" y1="8.6614" x2="9.0932" y2="8.9916" width="0.2286" layer="21"/>
<wire x1="9.0932" y1="8.9916" x2="9.0932" y2="7.9502" width="0.2286" layer="21"/>
<wire x1="9.0932" y1="7.9502" x2="8.7376" y2="7.9502" width="0.2286" layer="21"/>
<wire x1="9.0932" y1="7.9502" x2="9.4488" y2="7.9502" width="0.2286" layer="21"/>
<wire x1="-10.0584" y1="8.6868" x2="-9.7282" y2="9.017" width="0.2286" layer="21"/>
<wire x1="-9.7282" y1="9.017" x2="-9.7282" y2="7.9756" width="0.2286" layer="21"/>
<wire x1="-9.7282" y1="7.9756" x2="-10.0838" y2="7.9756" width="0.2286" layer="21"/>
<wire x1="-9.7282" y1="7.9756" x2="-9.3726" y2="7.9756" width="0.2286" layer="21"/>
<wire x1="-8.9154" y1="8.8392" x2="-8.9154" y2="8.1534" width="0.2286" layer="21"/>
<wire x1="-8.9154" y1="8.1534" x2="-8.7376" y2="7.9756" width="0.2286" layer="21"/>
<wire x1="-8.7376" y1="7.9756" x2="-8.382" y2="7.9756" width="0.2286" layer="21"/>
<wire x1="-8.382" y1="7.9756" x2="-8.2042" y2="8.1534" width="0.2286" layer="21"/>
<wire x1="-8.2042" y1="8.1534" x2="-8.2042" y2="8.8392" width="0.2286" layer="21"/>
<wire x1="-8.2042" y1="8.8392" x2="-8.382" y2="9.017" width="0.2286" layer="21"/>
<wire x1="-8.382" y1="9.017" x2="-8.7376" y2="9.017" width="0.2286" layer="21"/>
<wire x1="-8.7376" y1="9.017" x2="-8.9154" y2="8.8392" width="0.2286" layer="21"/>
<wire x1="-8.2296" y1="8.8138" x2="-8.89" y2="8.1534" width="0.2286" layer="21"/>
<wire x1="-9.8806" y1="-8.4582" x2="-9.5504" y2="-8.128" width="0.2286" layer="21"/>
<wire x1="-9.5504" y1="-8.128" x2="-9.5504" y2="-9.1694" width="0.2286" layer="21"/>
<wire x1="-9.5504" y1="-9.1694" x2="-9.906" y2="-9.1694" width="0.2286" layer="21"/>
<wire x1="-9.5504" y1="-9.1694" x2="-9.1948" y2="-9.1694" width="0.2286" layer="21"/>
<wire x1="-8.6868" y1="-8.4582" x2="-8.3566" y2="-8.128" width="0.2286" layer="21"/>
<wire x1="-8.3566" y1="-8.128" x2="-8.3566" y2="-9.1694" width="0.2286" layer="21"/>
<wire x1="-8.3566" y1="-9.1694" x2="-8.7122" y2="-9.1694" width="0.2286" layer="21"/>
<wire x1="-8.3566" y1="-9.1694" x2="-8.001" y2="-9.1694" width="0.2286" layer="21"/>
<wire x1="8.4582" y1="-8.1534" x2="8.636" y2="-7.9756" width="0.2286" layer="21"/>
<wire x1="8.636" y1="-7.9756" x2="8.9662" y2="-7.9756" width="0.2286" layer="21"/>
<wire x1="8.9662" y1="-7.9756" x2="9.144" y2="-8.1534" width="0.2286" layer="21"/>
<wire x1="9.144" y1="-8.1534" x2="9.144" y2="-8.3312" width="0.2286" layer="21"/>
<wire x1="9.144" y1="-8.3312" x2="8.4582" y2="-9.017" width="0.2286" layer="21"/>
<wire x1="8.4582" y1="-9.017" x2="9.144" y2="-9.017" width="0.2286" layer="21"/>
<wire x1="9.6266" y1="-8.1534" x2="9.8044" y2="-7.9756" width="0.2286" layer="21"/>
<wire x1="9.8044" y1="-7.9756" x2="10.1346" y2="-7.9756" width="0.2286" layer="21"/>
<wire x1="10.1346" y1="-7.9756" x2="10.3124" y2="-8.1534" width="0.2286" layer="21"/>
<wire x1="10.3124" y1="-8.1534" x2="10.3124" y2="-8.8392" width="0.2286" layer="21"/>
<wire x1="10.3124" y1="-8.8392" x2="10.1346" y2="-9.017" width="0.2286" layer="21"/>
<wire x1="10.1346" y1="-9.017" x2="9.779" y2="-9.017" width="0.2286" layer="21"/>
<wire x1="9.779" y1="-9.017" x2="9.6266" y2="-8.8646" width="0.2286" layer="21"/>
<wire x1="9.6266" y1="-8.8646" x2="9.6266" y2="-8.1534" width="0.2286" layer="21"/>
<wire x1="9.6774" y1="-8.7884" x2="10.287" y2="-8.1788" width="0.2286" layer="21"/>
<pad name="1" x="9" y="11" drill="0.7874" rot="R180"/>
<pad name="2" x="7" y="11" drill="0.7874" shape="octagon" rot="R180"/>
<pad name="3" x="5" y="11" drill="0.7874" shape="octagon" rot="R180"/>
<pad name="4" x="3" y="11" drill="0.7874" shape="octagon" rot="R180"/>
<pad name="5" x="1" y="11" drill="0.7874" shape="octagon" rot="R180"/>
<pad name="6" x="-1" y="11" drill="0.7874" shape="octagon" rot="R180"/>
<pad name="7" x="-3" y="11" drill="0.7874" shape="octagon" rot="R180"/>
<pad name="8" x="-5" y="11" drill="0.7874" shape="octagon" rot="R180"/>
<pad name="9" x="-7" y="11" drill="0.7874" shape="octagon" rot="R180"/>
<pad name="10" x="-9" y="11" drill="0.7874" shape="octagon" rot="R180"/>
<pad name="11" x="-9" y="-11" drill="0.7874" shape="octagon" rot="R180"/>
<pad name="12" x="-7" y="-11" drill="0.7874" shape="octagon" rot="R180"/>
<pad name="13" x="-5" y="-11" drill="0.7874" shape="octagon" rot="R180"/>
<pad name="14" x="-3" y="-11" drill="0.7874" shape="octagon" rot="R180"/>
<pad name="15" x="-1" y="-11" drill="0.7874" shape="octagon" rot="R180"/>
<pad name="16" x="1" y="-11" drill="0.7874" shape="octagon" rot="R180"/>
<pad name="17" x="3" y="-11" drill="0.7874" shape="octagon" rot="R180"/>
<pad name="18" x="5" y="-11" drill="0.7874" shape="octagon" rot="R180"/>
<pad name="19" x="7" y="-11" drill="0.7874" shape="octagon" rot="R180"/>
<pad name="20" x="9" y="-11" drill="0.7874" shape="octagon" rot="R180"/>
<text x="-2.8575" y="1.905" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
<text x="-3.4925" y="0.3175" size="1.27" layer="27" font="vector" ratio="18">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="NL-SW-LTE-S7588">
<wire x1="12.7" y1="-13.97" x2="-12.7" y2="-13.97" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="13.97" x2="-12.7" y2="-13.97" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-13.97" x2="12.7" y2="13.97" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="13.97" x2="12.7" y2="13.97" width="0.4064" layer="94"/>
<pin name="ADC1" x="15.24" y="8.89" length="short" swaplevel="1" rot="R180"/>
<pin name="CTS(O)" x="15.24" y="-8.89" length="short" direction="out" swaplevel="1" rot="R180"/>
<pin name="DIN" x="-15.24" y="6.35" length="short" swaplevel="1"/>
<pin name="DIO5" x="15.24" y="3.81" length="short" swaplevel="1" rot="R180"/>
<pin name="DIO7" x="15.24" y="6.35" length="short" swaplevel="1" rot="R180"/>
<pin name="DOUT" x="-15.24" y="8.89" length="short" direction="out" swaplevel="1"/>
<pin name="DTR(I)" x="-15.24" y="-8.89" length="short" direction="in" swaplevel="1"/>
<pin name="GND" x="-15.24" y="-11.43" length="short" direction="in" swaplevel="1"/>
<pin name="GND@1" x="15.24" y="-11.43" length="short" direction="in" swaplevel="1" rot="R180"/>
<pin name="GND@2" x="15.24" y="-1.27" length="short" direction="in" swaplevel="1" rot="R180"/>
<pin name="GND@3" x="-15.24" y="3.81" length="short" direction="in" swaplevel="1"/>
<pin name="PWR_ON" x="15.24" y="11.43" length="short" swaplevel="1" rot="R180"/>
<pin name="RESET" x="-15.24" y="1.27" length="short" direction="in" swaplevel="1"/>
<pin name="RTS(I)" x="15.24" y="1.27" length="short" direction="in" swaplevel="1" rot="R180"/>
<pin name="USB_D+" x="-15.24" y="-3.81" length="short" swaplevel="1"/>
<pin name="USB_D-" x="-15.24" y="-6.35" length="short" swaplevel="1"/>
<pin name="VCC" x="-15.24" y="11.43" length="short" direction="in" swaplevel="1"/>
<pin name="VGPIO" x="15.24" y="-6.35" length="short" direction="out" swaplevel="1" rot="R180"/>
<pin name="VREF" x="15.24" y="-3.81" length="short" direction="in" swaplevel="1" rot="R180"/>
<pin name="VUSB" x="-15.24" y="-1.27" length="short" direction="in" swaplevel="1"/>
<text x="-3.81" y="14.605" size="1.4224" layer="96" font="vector">&gt;VALUE</text>
<text x="-3.175" y="16.51" size="1.4224" layer="95" font="vector">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="NL-SW-LTE-S7588" prefix="IC">
<description>NAME:NL-SW-LTE-S7588;DESC:Skywire Dual ModeLTE CAT4 with HSPA+fallback, Verizon &amp; AT&amp;T -40+85C RoHS;SUP:www.nimbelink.com;VER:010</description>
<gates>
<gate name="G$1" symbol="NL-SW-LTE-S7588" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NL-SW-LTE-S7588">
<connects>
<connect gate="G$1" pin="ADC1" pad="19"/>
<connect gate="G$1" pin="CTS(O)" pad="12"/>
<connect gate="G$1" pin="DIN" pad="3"/>
<connect gate="G$1" pin="DIO5" pad="17"/>
<connect gate="G$1" pin="DIO7" pad="18"/>
<connect gate="G$1" pin="DOUT" pad="2"/>
<connect gate="G$1" pin="DTR(I)" pad="9"/>
<connect gate="G$1" pin="GND" pad="10"/>
<connect gate="G$1" pin="GND@1" pad="11"/>
<connect gate="G$1" pin="GND@2" pad="15"/>
<connect gate="G$1" pin="GND@3" pad="4"/>
<connect gate="G$1" pin="PWR_ON" pad="20"/>
<connect gate="G$1" pin="RESET" pad="5"/>
<connect gate="G$1" pin="RTS(I)" pad="16"/>
<connect gate="G$1" pin="USB_D+" pad="7"/>
<connect gate="G$1" pin="USB_D-" pad="8"/>
<connect gate="G$1" pin="VCC" pad="1"/>
<connect gate="G$1" pin="VGPIO" pad="13"/>
<connect gate="G$1" pin="VREF" pad="14"/>
<connect gate="G$1" pin="VUSB" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-S" package="NL-SW-LTE-S7588_S">
<connects>
<connect gate="G$1" pin="ADC1" pad="19"/>
<connect gate="G$1" pin="CTS(O)" pad="12"/>
<connect gate="G$1" pin="DIN" pad="3"/>
<connect gate="G$1" pin="DIO5" pad="17"/>
<connect gate="G$1" pin="DIO7" pad="18"/>
<connect gate="G$1" pin="DOUT" pad="2"/>
<connect gate="G$1" pin="DTR(I)" pad="9"/>
<connect gate="G$1" pin="GND" pad="10"/>
<connect gate="G$1" pin="GND@1" pad="11"/>
<connect gate="G$1" pin="GND@2" pad="15"/>
<connect gate="G$1" pin="GND@3" pad="4"/>
<connect gate="G$1" pin="PWR_ON" pad="20"/>
<connect gate="G$1" pin="RESET" pad="5"/>
<connect gate="G$1" pin="RTS(I)" pad="16"/>
<connect gate="G$1" pin="USB_D+" pad="7"/>
<connect gate="G$1" pin="USB_D-" pad="8"/>
<connect gate="G$1" pin="VCC" pad="1"/>
<connect gate="G$1" pin="VGPIO" pad="13"/>
<connect gate="G$1" pin="VREF" pad="14"/>
<connect gate="G$1" pin="VUSB" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="PCI_EXPRESS_MINI">
<packages>
<package name="PCI_EXPRESS_MINI">
<description>Manufacturer Part Number 
0679101002</description>
<wire x1="-7.9356" y1="2.4488" x2="-7.9356" y2="0.747" width="0.6096" layer="51"/>
<wire x1="-9" y1="0" x2="-0.75" y2="0" width="0" layer="20"/>
<wire x1="0.75" y1="0" x2="16.7" y2="0" width="0" layer="20"/>
<wire x1="18.85" y1="3.2" x2="18.85" y2="50.95" width="0" layer="20"/>
<wire x1="18.85" y1="50.95" x2="-11.15" y2="50.95" width="0" layer="20"/>
<wire x1="-11.15" y1="50.95" x2="-11.15" y2="3.2" width="0" layer="20"/>
<wire x1="-0.75" y1="0" x2="-0.75" y2="3.25" width="0" layer="20"/>
<wire x1="0.75" y1="3.25" x2="0.75" y2="0" width="0" layer="20"/>
<wire x1="-0.75" y1="3.25" x2="0.75" y2="3.25" width="0" layer="20" curve="-180"/>
<wire x1="-9.8" y1="3.2" x2="-9" y2="2.4" width="0" layer="20" curve="-90"/>
<wire x1="16.7" y1="2.4" x2="17.5" y2="3.2" width="0" layer="20" curve="-90"/>
<wire x1="17.5" y1="3.2" x2="18.85" y2="3.2" width="0" layer="20"/>
<wire x1="16.7" y1="2.4" x2="16.7" y2="0" width="0" layer="20"/>
<wire x1="-9.8" y1="3.2" x2="-11.15" y2="3.2" width="0" layer="20"/>
<wire x1="-9" y1="2.4" x2="-9" y2="0" width="0" layer="20"/>
<smd name="1" x="-7.95" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="2" x="-7.55" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="3" x="-7.15" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="4" x="-6.75" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="5" x="-6.35" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="6" x="-5.95" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="7" x="-5.55" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="8" x="-5.15" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="9" x="-4.75" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="10" x="-4.35" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="11" x="-3.95" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="12" x="-3.55" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="13" x="-3.15" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="14" x="-2.75" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="15" x="-2.35" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="16" x="-1.95" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="17" x="1.65" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="18" x="2.05" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="19" x="2.45" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="20" x="2.85" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="21" x="3.25" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="22" x="3.65" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="23" x="4.05" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="24" x="4.45" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="25" x="4.85" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="26" x="5.25" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="27" x="5.65" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="28" x="6.05" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="29" x="6.45" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="30" x="6.85" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="31" x="7.25" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="32" x="7.65" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="33" x="8.05" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="34" x="8.45" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="35" x="8.85" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="36" x="9.25" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="37" x="9.65" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="38" x="10.05" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="39" x="10.45" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="40" x="10.85" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="41" x="11.25" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="42" x="11.65" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="43" x="12.05" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="44" x="12.45" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="45" x="12.85" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="46" x="13.25" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="47" x="13.65" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="48" x="14.05" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="49" x="14.45" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="50" x="14.85" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<smd name="51" x="15.25" y="1.6" dx="0.6" dy="2.3" layer="1"/>
<smd name="52" x="15.65" y="1.6" dx="0.6" dy="2.3" layer="16" rot="R180"/>
<pad name="P1" x="-8.25" y="48.05" drill="2.6" diameter="4.92" shape="square"/>
<pad name="P2" x="15.95" y="48.05" drill="2.6" diameter="4.92" shape="square"/>
<text x="1.5875" y="13.335" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
<text x="0.9525" y="11.7475" size="1.27" layer="27" font="vector" ratio="18">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="PCI_EXPRESS_MINI">
<wire x1="20.32" y1="-36.83" x2="-20.32" y2="-36.83" width="0.4064" layer="94"/>
<wire x1="-20.32" y1="36.83" x2="-20.32" y2="13.97" width="0.4064" layer="94"/>
<wire x1="-20.32" y1="13.97" x2="-20.32" y2="-36.83" width="0.4064" layer="94"/>
<wire x1="20.32" y1="-36.83" x2="20.32" y2="13.97" width="0.4064" layer="94"/>
<wire x1="20.32" y1="13.97" x2="20.32" y2="36.83" width="0.4064" layer="94"/>
<wire x1="-20.32" y1="36.83" x2="20.32" y2="36.83" width="0.4064" layer="94"/>
<wire x1="-20.32" y1="13.97" x2="20.32" y2="13.97" width="0.254" layer="94"/>
<pin name="+1.5V" x="22.86" y="-1.27" length="short" direction="pas" rot="R180"/>
<pin name="+1.5V@1" x="22.86" y="-26.67" length="short" direction="pas" rot="R180"/>
<pin name="+3.3VAUX@1" x="-22.86" y="-16.51" length="short" direction="pas"/>
<pin name="+3.3VAUX@2" x="-22.86" y="-19.05" length="short" direction="pas"/>
<pin name="+3.3VAUX@3" x="22.86" y="3.81" length="short" direction="pas" rot="R180"/>
<pin name="+3.3VAUX@4" x="22.86" y="-31.75" length="short" direction="pas" rot="R180"/>
<pin name="1.5V" x="22.86" y="29.21" length="short" direction="pas" rot="R180"/>
<pin name="3.3VAUX" x="22.86" y="34.29" length="short" direction="pas" rot="R180"/>
<pin name="CLKREQ#" x="-22.86" y="26.67" length="short" direction="pas"/>
<pin name="COEX1" x="-22.86" y="31.75" length="short" direction="pas"/>
<pin name="COEX2" x="-22.86" y="29.21" length="short" direction="pas"/>
<pin name="GND" x="-22.86" y="24.13" length="short" direction="pas"/>
<pin name="GND@1" x="22.86" y="31.75" length="short" direction="pas" rot="R180"/>
<pin name="GND@2" x="-22.86" y="16.51" length="short" direction="pas"/>
<pin name="GND@3" x="-22.86" y="6.35" length="short" direction="pas"/>
<pin name="GND@4" x="-22.86" y="-1.27" length="short" direction="pas"/>
<pin name="GND@5" x="-22.86" y="-3.81" length="short" direction="pas"/>
<pin name="GND@6" x="-22.86" y="-11.43" length="short" direction="pas"/>
<pin name="GND@7" x="-22.86" y="-13.97" length="short" direction="pas"/>
<pin name="GND@8" x="-22.86" y="-21.59" length="short" direction="pas"/>
<pin name="GND@9" x="22.86" y="11.43" length="short" direction="pas" rot="R180"/>
<pin name="GND@10" x="22.86" y="1.27" length="short" direction="pas" rot="R180"/>
<pin name="GND@11" x="22.86" y="-8.89" length="short" direction="pas" rot="R180"/>
<pin name="GND@12" x="22.86" y="-16.51" length="short" direction="pas" rot="R180"/>
<pin name="GND@13" x="22.86" y="-29.21" length="short" direction="pas" rot="R180"/>
<pin name="LED_WLAN#" x="22.86" y="-21.59" length="short" direction="pas" rot="R180"/>
<pin name="LED_WPAN#" x="22.86" y="-24.13" length="short" direction="pas" rot="R180"/>
<pin name="LED_WWAN#" x="22.86" y="-19.05" length="short" direction="pas" rot="R180"/>
<pin name="PE" x="-22.86" y="-34.29" length="short" direction="pas"/>
<pin name="PE@1" x="22.86" y="-34.29" length="short" direction="pas" rot="R180"/>
<pin name="PERN0" x="-22.86" y="3.81" length="short" direction="pas"/>
<pin name="PERP0" x="-22.86" y="1.27" length="short" direction="pas"/>
<pin name="PERST#" x="22.86" y="6.35" length="short" direction="pas" rot="R180"/>
<pin name="PETN0" x="-22.86" y="-6.35" length="short" direction="pas"/>
<pin name="PETP0" x="-22.86" y="-8.89" length="short" direction="pas"/>
<pin name="REFCLK+" x="-22.86" y="19.05" length="short" direction="pas"/>
<pin name="REFCLK-" x="-22.86" y="21.59" length="short" direction="pas"/>
<pin name="RESERVED" x="-22.86" y="-24.13" length="short" direction="pas"/>
<pin name="RESERVED(UIM_C4)" x="-22.86" y="8.89" length="short" direction="pas"/>
<pin name="RESERVED(UIM_C8)" x="-22.86" y="11.43" length="short" direction="pas"/>
<pin name="RESERVED@1" x="-22.86" y="-26.67" length="short" direction="pas"/>
<pin name="RESERVED@2" x="-22.86" y="-29.21" length="short" direction="pas"/>
<pin name="RESERVED@3" x="-22.86" y="-31.75" length="short" direction="pas"/>
<pin name="SMB_CLK" x="22.86" y="-3.81" length="short" direction="pas" rot="R180"/>
<pin name="SMB_DATA" x="22.86" y="-6.35" length="short" direction="pas" rot="R180"/>
<pin name="UIM_CLK" x="22.86" y="21.59" length="short" direction="pas" rot="R180"/>
<pin name="UIM_DATA" x="22.86" y="24.13" length="short" direction="pas" rot="R180"/>
<pin name="UIM_PWR" x="22.86" y="26.67" length="short" direction="pas" rot="R180"/>
<pin name="UIM_RESET" x="22.86" y="19.05" length="short" direction="pas" rot="R180"/>
<pin name="UIM_VPP" x="22.86" y="16.51" length="short" direction="pas" rot="R180"/>
<pin name="USB_D+" x="22.86" y="-13.97" length="short" direction="pas" rot="R180"/>
<pin name="USB_D-" x="22.86" y="-11.43" length="short" direction="pas" rot="R180"/>
<pin name="WAKE#" x="-22.86" y="34.29" length="short" direction="pas"/>
<pin name="W_DISABLE#" x="22.86" y="8.89" length="short" direction="pas" rot="R180"/>
<text x="-3.81" y="37.465" size="1.4224" layer="96" font="vector">&gt;VALUE</text>
<text x="-3.175" y="39.37" size="1.4224" layer="95" font="vector">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PCI_EXPRESS_MINI" prefix="X">
<description>NAME:PCI_EXPRESS_MINI;DESC:PCI Express Mini Card FULL;SUP:www.bipom.com;VER:005</description>
<gates>
<gate name="G$1" symbol="PCI_EXPRESS_MINI" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="PCI_EXPRESS_MINI">
<connects>
<connect gate="G$1" pin="+1.5V" pad="28"/>
<connect gate="G$1" pin="+1.5V@1" pad="48"/>
<connect gate="G$1" pin="+3.3VAUX@1" pad="39"/>
<connect gate="G$1" pin="+3.3VAUX@2" pad="41"/>
<connect gate="G$1" pin="+3.3VAUX@3" pad="24"/>
<connect gate="G$1" pin="+3.3VAUX@4" pad="52"/>
<connect gate="G$1" pin="1.5V" pad="6"/>
<connect gate="G$1" pin="3.3VAUX" pad="2"/>
<connect gate="G$1" pin="CLKREQ#" pad="7"/>
<connect gate="G$1" pin="COEX1" pad="3"/>
<connect gate="G$1" pin="COEX2" pad="5"/>
<connect gate="G$1" pin="GND" pad="9"/>
<connect gate="G$1" pin="GND@1" pad="4"/>
<connect gate="G$1" pin="GND@10" pad="26"/>
<connect gate="G$1" pin="GND@11" pad="34"/>
<connect gate="G$1" pin="GND@12" pad="40"/>
<connect gate="G$1" pin="GND@13" pad="50"/>
<connect gate="G$1" pin="GND@2" pad="15"/>
<connect gate="G$1" pin="GND@3" pad="21"/>
<connect gate="G$1" pin="GND@4" pad="27"/>
<connect gate="G$1" pin="GND@5" pad="29"/>
<connect gate="G$1" pin="GND@6" pad="35"/>
<connect gate="G$1" pin="GND@7" pad="37"/>
<connect gate="G$1" pin="GND@8" pad="43"/>
<connect gate="G$1" pin="GND@9" pad="18"/>
<connect gate="G$1" pin="LED_WLAN#" pad="44"/>
<connect gate="G$1" pin="LED_WPAN#" pad="46"/>
<connect gate="G$1" pin="LED_WWAN#" pad="42"/>
<connect gate="G$1" pin="PE" pad="P1"/>
<connect gate="G$1" pin="PE@1" pad="P2"/>
<connect gate="G$1" pin="PERN0" pad="23"/>
<connect gate="G$1" pin="PERP0" pad="25"/>
<connect gate="G$1" pin="PERST#" pad="22"/>
<connect gate="G$1" pin="PETN0" pad="31"/>
<connect gate="G$1" pin="PETP0" pad="33"/>
<connect gate="G$1" pin="REFCLK+" pad="13"/>
<connect gate="G$1" pin="REFCLK-" pad="11"/>
<connect gate="G$1" pin="RESERVED" pad="45"/>
<connect gate="G$1" pin="RESERVED(UIM_C4)" pad="19"/>
<connect gate="G$1" pin="RESERVED(UIM_C8)" pad="17"/>
<connect gate="G$1" pin="RESERVED@1" pad="47"/>
<connect gate="G$1" pin="RESERVED@2" pad="49"/>
<connect gate="G$1" pin="RESERVED@3" pad="51"/>
<connect gate="G$1" pin="SMB_CLK" pad="30"/>
<connect gate="G$1" pin="SMB_DATA" pad="32"/>
<connect gate="G$1" pin="UIM_CLK" pad="12"/>
<connect gate="G$1" pin="UIM_DATA" pad="10"/>
<connect gate="G$1" pin="UIM_PWR" pad="8"/>
<connect gate="G$1" pin="UIM_RESET" pad="14"/>
<connect gate="G$1" pin="UIM_VPP" pad="16"/>
<connect gate="G$1" pin="USB_D+" pad="38"/>
<connect gate="G$1" pin="USB_D-" pad="36"/>
<connect gate="G$1" pin="WAKE#" pad="1"/>
<connect gate="G$1" pin="W_DISABLE#" pad="20"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="1N4148W-7-F">
<packages>
<package name="1N4148W-7-F">
<wire x1="1.6256" y1="-1.0668" x2="-1.6256" y2="-1.0668" width="0.4064" layer="21"/>
<wire x1="-1.6256" y1="1.0668" x2="1.6256" y2="1.0668" width="0.4064" layer="21"/>
<wire x1="0.4762" y1="-0.3969" x2="0.4762" y2="0.397" width="0.4064" layer="21"/>
<wire x1="0.4762" y1="0.397" x2="-0.4763" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.4763" y1="0" x2="0.4762" y2="-0.3969" width="0.4064" layer="21"/>
<wire x1="-0.4763" y1="-0.397" x2="-0.4763" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.4763" y1="0" x2="-0.4763" y2="0.3969" width="0.4064" layer="21"/>
<wire x1="-1.6256" y1="0.4128" x2="-1.6256" y2="-0.4128" width="0.4064" layer="51"/>
<wire x1="1.6256" y1="-1.0668" x2="1.6256" y2="-0.9652" width="0.4064" layer="21"/>
<wire x1="-1.6256" y1="-1.0668" x2="-1.6256" y2="-0.9652" width="0.4064" layer="21"/>
<wire x1="-1.6256" y1="1.0668" x2="-1.6256" y2="0.9652" width="0.4064" layer="21"/>
<wire x1="1.6256" y1="1.0668" x2="1.6256" y2="0.9652" width="0.4064" layer="21"/>
<smd name="A" x="1.6256" y="0" dx="1.016" dy="1.27" layer="1" rot="R180"/>
<smd name="C" x="-1.6256" y="0" dx="1.016" dy="1.27" layer="1" rot="R180"/>
<text x="-2.8575" y="3.175" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
<text x="-3.4925" y="1.5875" size="1.27" layer="27" font="vector" ratio="18">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="DIODE">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-3.175" y="3.81" size="1.4224" layer="95" font="vector">&gt;NAME</text>
<text x="-3.81" y="1.905" size="1.4224" layer="96" font="vector">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="1N4148W-7-F" prefix="D">
<description>NAME:1N4148W-7-F;DESC:DIODE SWITCH 100V 0.15A SOD123 -65+150C RoHS;SUP:Digi-Key 1N4148W-FDICT-ND;VER:002</description>
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1N4148W-7-F">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ERJ-3EKF1001V">
<packages>
<package name="ERJ-3EKF1001V">
<wire x1="-1.5812" y1="0.7938" x2="1.5811" y2="0.7938" width="0.4064" layer="21"/>
<wire x1="1.5812" y1="-0.7938" x2="-1.5812" y2="-0.7938" width="0.4064" layer="21"/>
<wire x1="-1.5875" y1="0.7875" x2="-1.5875" y2="-0.7874" width="0.4064" layer="21"/>
<wire x1="1.5875" y1="0.7875" x2="1.5875" y2="-0.7874" width="0.4064" layer="21"/>
<smd name="1" x="-0.7874" y="0" dx="0.762" dy="0.9652" layer="1"/>
<smd name="2" x="0.7874" y="0" dx="0.762" dy="0.9652" layer="1"/>
<text x="-3.175" y="1.27" size="1.27" layer="27" font="vector" ratio="18">&gt;VALUE</text>
<text x="-2.54" y="2.8575" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="R">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.175" y="3.175" size="1.4224" layer="95" font="vector">&gt;NAME</text>
<text x="-3.81" y="1.27" size="1.4224" layer="96" font="vector">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ERJ-3EKF1001V" prefix="R">
<description>NAME:ERJ-3EKF1001V;DESC:RES 1.00K OHM 1/10W 1% 0603 SMD -55+155C RoHS;SUP:Digi-Key P1.00KHCT-ND;VER:003</description>
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ERJ-3EKF1001V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="FDV301N">
<packages>
<package name="FDV301N">
<wire x1="-0.9462" y1="0.9525" x2="-1.5875" y2="0.9525" width="0.4064" layer="21"/>
<wire x1="-1.5875" y1="0.9525" x2="-1.5875" y2="-0.0063" width="0.4064" layer="21"/>
<wire x1="-0.0063" y1="-0.635" x2="0.0063" y2="-0.635" width="0.4064" layer="21"/>
<wire x1="0.9462" y1="0.9525" x2="1.5875" y2="0.9525" width="0.4064" layer="21"/>
<wire x1="1.5875" y1="0.9525" x2="1.5875" y2="-0.0063" width="0.4064" layer="21"/>
<smd name="D" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="G" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="S" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-2.54" y="3.81" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
<text x="-3.175" y="2.2225" size="1.27" layer="27" font="vector" ratio="18">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="N-CH">
<circle x="2.54" y="2.54" radius="0.3175" width="0" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.3175" width="0" layer="94"/>
<circle x="1.27" y="-2.54" radius="0.3175" width="0" layer="94"/>
<wire x1="-3.81" y1="-2.54" x2="-2.4892" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.762" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.683" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="1.397" width="0.254" layer="94"/>
<wire x1="0.635" y1="0.635" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="0.635" y1="-0.635" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-1.397" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="-3.683" width="0.254" layer="94"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0.635" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-0.635" x2="1.905" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-0.635" x2="2.54" y2="0.635" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="3.175" y1="0.635" x2="1.905" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="2.2225" y2="2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<pin name="D-N" x="3.81" y="2.54" visible="off" length="point" direction="in" rot="R180"/>
<pin name="G-N" x="-6.35" y="-2.54" visible="off" length="short" direction="in"/>
<pin name="S-N" x="3.81" y="-2.54" visible="off" length="point" direction="in" rot="R180"/>
<text x="-5.715" y="4.445" size="1.4224" layer="96" font="vector">&gt;VALUE</text>
<text x="-5.08" y="6.35" size="1.4224" layer="95" font="vector">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="FDV301N" prefix="M">
<description>NAME:FDV301N;DESC:MOSFET N-CH 25V 220MA SOT-23 -55+150C RoHS;SUP:Digi-Key FDV301NCT-ND;VER:001</description>
<gates>
<gate name="G$1" symbol="N-CH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FDV301N">
<connects>
<connect gate="G$1" pin="D-N" pad="D"/>
<connect gate="G$1" pin="G-N" pad="G"/>
<connect gate="G$1" pin="S-N" pad="S"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="NPPN101BFLC-RC">
<packages>
<package name="NPPN101BFLC-RC">
</package>
</packages>
<symbols>
<symbol name="SOCKET">
<wire x1="-0.6477" y1="1.6129" x2="0.6223" y2="1.6129" width="0.254" layer="94"/>
<wire x1="0.6223" y1="1.6129" x2="0.6223" y2="-1.5621" width="0.254" layer="94"/>
<wire x1="0.6223" y1="-1.5621" x2="-0.6477" y2="-1.5621" width="0.254" layer="94"/>
<wire x1="-0.6477" y1="-1.5621" x2="-0.6477" y2="1.6129" width="0.254" layer="94"/>
<wire x1="-0.3302" y1="0.9779" x2="0.3048" y2="0.9779" width="0.254" layer="94"/>
<wire x1="-0.3302" y1="0.3429" x2="0.3048" y2="0.3429" width="0.254" layer="94"/>
<wire x1="-0.3302" y1="-0.2921" x2="0.3048" y2="-0.2921" width="0.254" layer="94"/>
<wire x1="-0.3302" y1="-0.9271" x2="0.3048" y2="-0.9271" width="0.254" layer="94"/>
<text x="-3.4925" y="1.905" size="1.4224" layer="96" font="vector">&gt;VALUE</text>
<text x="-3.175" y="3.81" size="1.4224" layer="95" font="vector">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="NPPN101BFLC-RC" prefix="SOCKET">
<description>NAME:NPPN101BFLC-RC;DESC:10 Position Header Connector 0.079" (2.00mm) Surface Mount Gold -40+105C RoHS;SUP:Digi-Key S5901-10-ND;VER:001</description>
<gates>
<gate name="SOCKET$1" symbol="SOCKET" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NPPN101BFLC-RC">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="TPS61230DRCR">
<packages>
<package name="TPS61230DRCR">
<wire x1="-1" y1="-1.143" x2="-1" y2="-1.8225" width="0.3048" layer="51"/>
<wire x1="-1.7526" y1="-1.4478" x2="-1.7526" y2="1.7526" width="0.4064" layer="21"/>
<wire x1="-1.7526" y1="1.7526" x2="-1.4478" y2="1.7526" width="0.4064" layer="21"/>
<wire x1="1.4478" y1="1.7526" x2="1.7526" y2="1.7526" width="0.4064" layer="21"/>
<wire x1="-1.7526" y1="-1.4478" x2="-1.4478" y2="-1.7526" width="0.4064" layer="21"/>
<wire x1="1.4478" y1="-1.7526" x2="1.7526" y2="-1.7526" width="0.4064" layer="21"/>
<wire x1="1.7526" y1="-1.7526" x2="1.7526" y2="1.7526" width="0.4064" layer="21"/>
<smd name="1" x="-1" y="-1.4625" dx="0.28" dy="0.825" layer="1" roundness="100"/>
<smd name="2" x="-0.5" y="-1.4625" dx="0.28" dy="0.825" layer="1" roundness="100"/>
<smd name="3" x="0" y="-1.4625" dx="0.28" dy="0.825" layer="1" roundness="100"/>
<smd name="4" x="0.5" y="-1.4625" dx="0.28" dy="0.825" layer="1" roundness="100"/>
<smd name="5" x="1" y="-1.4625" dx="0.28" dy="0.825" layer="1" roundness="100"/>
<smd name="6" x="1" y="1.4625" dx="0.28" dy="0.825" layer="1" roundness="100"/>
<smd name="7" x="0.5" y="1.4625" dx="0.28" dy="0.825" layer="1" roundness="100"/>
<smd name="8" x="0" y="1.4625" dx="0.28" dy="0.825" layer="1" roundness="100"/>
<smd name="9" x="-0.5" y="1.4625" dx="0.28" dy="0.825" layer="1" roundness="100"/>
<smd name="10" x="-1" y="1.4625" dx="0.28" dy="0.825" layer="1" roundness="100"/>
<text x="-2.54" y="4.1275" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
<text x="-3.175" y="2.54" size="1.27" layer="27" font="vector" ratio="18">&gt;VALUE</text>
<smd name="P$1" x="0" y="0" dx="2.15" dy="1.6" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="TPS61230DRCR">
<wire x1="-11.43" y1="15.24" x2="11.43" y2="15.24" width="0.4064" layer="94"/>
<wire x1="11.43" y1="-20.32" x2="11.43" y2="15.24" width="0.4064" layer="94"/>
<wire x1="11.43" y1="-20.32" x2="-11.43" y2="-20.32" width="0.4064" layer="94"/>
<wire x1="-11.43" y1="15.24" x2="-11.43" y2="-20.32" width="0.4064" layer="94"/>
<pin name="EN" x="-13.97" y="-7.62" length="short"/>
<pin name="FB" x="13.97" y="-2.54" length="short" rot="R180"/>
<pin name="HYS" x="-13.97" y="-12.7" length="short"/>
<pin name="PAD" x="13.97" y="-17.78" length="short" rot="R180"/>
<pin name="PG" x="13.97" y="-8.89" length="short" rot="R180"/>
<pin name="SS" x="-13.97" y="-17.78" length="short"/>
<pin name="SW" x="-13.97" y="12.7" length="short"/>
<pin name="SW@1" x="-13.97" y="10.16" length="short"/>
<pin name="VIN" x="-13.97" y="6.35" length="short"/>
<pin name="VOUT" x="13.97" y="12.7" length="short" rot="R180"/>
<pin name="VOUT@1" x="13.97" y="10.16" length="short" rot="R180"/>
<text x="0" y="17.78" size="1.4224" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="15.875" size="1.4224" layer="96" font="vector" align="bottom-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TPS61230DRCR" prefix="IC">
<description>NAME:TPS61230DRCR;DESC:Boost Switching Regulator IC Positive Adjustable 2.5V 1 Output 4A (Switch) 10-VFDFN Exposed Pad -40+150C RoHS;SUP:Digi-Key 296-37761-1-ND;VER:001</description>
<gates>
<gate name="G$1" symbol="TPS61230DRCR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TPS61230DRCR">
<connects>
<connect gate="G$1" pin="EN" pad="9"/>
<connect gate="G$1" pin="FB" pad="7"/>
<connect gate="G$1" pin="HYS" pad="8"/>
<connect gate="G$1" pin="PAD" pad="P$1"/>
<connect gate="G$1" pin="PG" pad="5"/>
<connect gate="G$1" pin="SS" pad="6"/>
<connect gate="G$1" pin="SW" pad="1"/>
<connect gate="G$1" pin="SW@1" pad="2"/>
<connect gate="G$1" pin="VIN" pad="10"/>
<connect gate="G$1" pin="VOUT" pad="3"/>
<connect gate="G$1" pin="VOUT@1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="C1608X5R1A226M080AC">
<packages>
<package name="C1608X5R1A226M080AC">
<wire x1="-1.5812" y1="0.7938" x2="1.5811" y2="0.7938" width="0.4064" layer="21"/>
<wire x1="1.5812" y1="-0.7938" x2="-1.5812" y2="-0.7938" width="0.4064" layer="21"/>
<wire x1="-1.5875" y1="0.7875" x2="-1.5875" y2="-0.7874" width="0.4064" layer="21"/>
<wire x1="1.5875" y1="0.7875" x2="1.5875" y2="-0.7874" width="0.4064" layer="21"/>
<smd name="1" x="-0.7874" y="0" dx="0.762" dy="0.9652" layer="1"/>
<smd name="2" x="0.7874" y="0" dx="0.762" dy="0.9652" layer="1"/>
<text x="-3.175" y="1.27" size="1.27" layer="27" font="vector" ratio="18">&gt;VALUE</text>
<text x="-2.54" y="2.8575" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="C">
<rectangle x1="-1.27" y1="-1.524" x2="-0.508" y2="1.524" layer="94"/>
<rectangle x1="0.508" y1="-1.524" x2="1.27" y2="1.524" layer="94"/>
<pin name="1" x="-3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.4224" layer="95" font="vector">&gt;NAME</text>
<text x="-3.81" y="1.905" size="1.4224" layer="96" font="vector">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="C1608X5R1A226M080AC" prefix="C">
<description>NAME:C1608X5R1A226M080AC;DESC:CAP CER 22UF 10V 10% X5R 0603 -55+85C RoHS;SUP:Digi-Key 445-9077-1-ND;VER:001</description>
<gates>
<gate name="1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C1608X5R1A226M080AC">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SPM5030T-1R0M">
<packages>
<package name="SPM5030T-1R0M">
<wire x1="-2.95" y1="1.4114" x2="-2.95" y2="2.95" width="0.4064" layer="21"/>
<wire x1="-2.95" y1="2.95" x2="2.95" y2="2.95" width="0.4064" layer="21"/>
<wire x1="2.95" y1="2.95" x2="2.95" y2="1.4114" width="0.4064" layer="21"/>
<wire x1="2.95" y1="-1.4114" x2="2.95" y2="-2.95" width="0.4064" layer="21"/>
<wire x1="2.95" y1="-2.95" x2="-2.95" y2="-2.95" width="0.4064" layer="21"/>
<wire x1="-2.95" y1="-2.95" x2="-2.95" y2="-1.4114" width="0.4064" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="1.5" dy="2.2" layer="1"/>
<smd name="2" x="2.1" y="0" dx="1.5" dy="2.2" layer="1"/>
<text x="-2.8575" y="5.08" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
<text x="-3.4925" y="3.4925" size="1.27" layer="27" font="vector" ratio="18">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="L">
<wire x1="-2.54" y1="1.27" x2="-3.81" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="1.27" y1="0" x2="0" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="2.54" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="3.81" y1="0" x2="2.54" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="-2.54" y1="1.27" x2="-3.81" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="1.27" y1="0" x2="0" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="2.54" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="3.81" y1="0" x2="2.54" y2="1.27" width="0.254" layer="94" curve="90"/>
<pin name="1" x="-6.35" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="6.35" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.4224" layer="95" font="vector">&gt;NAME</text>
<text x="-3.81" y="1.905" size="1.4224" layer="96" font="vector">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SPM5030T-1R0M" prefix="L">
<description>NAME:SPM5030T-1R0M;DESC:FIXED INDUCTOR 1uH 10.1A 11.44 mOhm Max SMD -40+125C RoHS;SUP:Digi-Key 445-15785-1-ND;VER:002</description>
<gates>
<gate name="G$1" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SPM5030T-1R0M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ERJ-3EKF1003V">
<packages>
<package name="ERJ-3EKF1003V">
<wire x1="-1.5812" y1="0.7938" x2="1.5811" y2="0.7938" width="0.4064" layer="21"/>
<wire x1="1.5812" y1="-0.7938" x2="-1.5812" y2="-0.7938" width="0.4064" layer="21"/>
<wire x1="-1.5875" y1="0.7875" x2="-1.5875" y2="-0.7874" width="0.4064" layer="21"/>
<wire x1="1.5875" y1="0.7875" x2="1.5875" y2="-0.7874" width="0.4064" layer="21"/>
<smd name="1" x="-0.7874" y="0" dx="0.762" dy="0.9652" layer="1"/>
<smd name="2" x="0.7874" y="0" dx="0.762" dy="0.9652" layer="1"/>
<text x="-2.413" y="1.5875" size="1.27" layer="27" font="vector" ratio="18">&gt;VALUE</text>
<text x="-2.413" y="3.4925" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="R">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-1.27" y="1.4986" size="1.4224" layer="95" font="vector">&gt;NAME</text>
<text x="-1.27" y="-2.667" size="1.4224" layer="96" font="vector">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ERJ-3EKF1003V" prefix="R">
<description>NAME:ERJ-3EKF1003V;DESC:RES 100K OHM 1/10W 1% 0603 SMD SMD INDUSTRIAL RoHS;SUP:Digi-Key P100KHCT-ND;VER:003</description>
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ERJ-3EKF1003V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="C0603C103K5RACTU">
<packages>
<package name="C0603C103K5RACTU">
<wire x1="-1.5812" y1="0.7938" x2="1.5811" y2="0.7938" width="0.4064" layer="21"/>
<wire x1="1.5812" y1="-0.7938" x2="-1.5812" y2="-0.7938" width="0.4064" layer="21"/>
<wire x1="-1.5875" y1="0.7875" x2="-1.5875" y2="-0.7874" width="0.4064" layer="21"/>
<wire x1="1.5875" y1="0.7875" x2="1.5875" y2="-0.7874" width="0.4064" layer="21"/>
<smd name="1" x="-0.7874" y="0" dx="0.762" dy="0.9652" layer="1"/>
<smd name="2" x="0.7874" y="0" dx="0.762" dy="0.9652" layer="1"/>
<text x="-2.413" y="-2.8575" size="1.27" layer="27" font="vector" ratio="18">&gt;VALUE</text>
<text x="-2.413" y="2.2225" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="C">
<text x="-1.27" y="3.175" size="1.4224" layer="95" font="vector">&gt;NAME</text>
<text x="-1.27" y="-4.699" size="1.4224" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-1.524" x2="-0.508" y2="1.524" layer="94"/>
<rectangle x1="0.508" y1="-1.524" x2="1.27" y2="1.524" layer="94"/>
<pin name="1" x="-3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C0603C103K5RACTU" prefix="C">
<description>NAME:C0603C103K5RACTU;DESC:CAP 10000PF 50V CERAMIC X7R 0603 INDUSTRIAL RoHS;SUP:Digi-Key 399-1091-1-ND;VER:003</description>
<gates>
<gate name="1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C0603C103K5RACTU">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ERJ-3EKF3003V">
<packages>
<package name="ERJ-3EKF3003V">
<wire x1="-1.5812" y1="0.7938" x2="1.5811" y2="0.7938" width="0.4064" layer="21"/>
<wire x1="1.5812" y1="-0.7938" x2="-1.5812" y2="-0.7938" width="0.4064" layer="21"/>
<wire x1="-1.5875" y1="0.7875" x2="-1.5875" y2="-0.7874" width="0.4064" layer="21"/>
<wire x1="1.5875" y1="0.7875" x2="1.5875" y2="-0.7874" width="0.4064" layer="21"/>
<smd name="1" x="-0.7874" y="0" dx="0.762" dy="0.9652" layer="1"/>
<smd name="2" x="0.7874" y="0" dx="0.762" dy="0.9652" layer="1"/>
<text x="-3.175" y="1.27" size="1.27" layer="27" font="vector" ratio="18">&gt;VALUE</text>
<text x="-2.54" y="2.8575" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="R">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.175" y="3.175" size="1.4224" layer="95" font="vector">&gt;NAME</text>
<text x="-3.81" y="1.27" size="1.4224" layer="96" font="vector">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ERJ-3EKF3003V" prefix="R">
<description>NAME:ERJ-3EKF3003V;DESC:RES SMD 300K OHM 1% 1/10W 0603 SMD -55+155C RoHS;SUP:Digi-Key P300KHCT-ND;VER:002</description>
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ERJ-3EKF3003V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="C1608C0G1H180J">
<packages>
<package name="C1608C0G1H180J">
<wire x1="-1.5812" y1="0.7938" x2="1.5811" y2="0.7938" width="0.4064" layer="21"/>
<wire x1="1.5812" y1="-0.7938" x2="-1.5812" y2="-0.7938" width="0.4064" layer="21"/>
<wire x1="-1.5875" y1="0.7875" x2="-1.5875" y2="-0.7874" width="0.4064" layer="21"/>
<wire x1="1.5875" y1="0.7875" x2="1.5875" y2="-0.7874" width="0.4064" layer="21"/>
<smd name="1" x="-0.7874" y="0" dx="0.762" dy="0.9652" layer="1"/>
<smd name="2" x="0.7874" y="0" dx="0.762" dy="0.9652" layer="1"/>
<text x="-2.413" y="1.5875" size="1.27" layer="27" font="vector" ratio="18">&gt;VALUE</text>
<text x="-2.413" y="3.4925" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="C">
<text x="-3.175" y="4.445" size="1.4224" layer="95" font="vector">&gt;NAME</text>
<text x="-3.81" y="2.286" size="1.4224" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-1.524" x2="-0.508" y2="1.524" layer="94"/>
<rectangle x1="0.508" y1="-1.524" x2="1.27" y2="1.524" layer="94"/>
<pin name="1" x="-3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C1608C0G1H180J" prefix="C">
<description>NAME:C1608C0G1H180J;DESC:CAP CER 18PF 50V C0G 5% 0603 INDUSTRIAL RoHS;SUP:Digi-Key 445-1272-1-ND;VER:003</description>
<gates>
<gate name="1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C1608C0G1H180J">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="THPAD">
<packages>
<package name="PAD125-250">
<pad name="1" x="0" y="0" drill="3.175" diameter="6.35"/>
<text x="3.81" y="-0.3175" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
<package name="PAD016">
<pad name="1" x="0" y="0" drill="0.4064" diameter="0.8128"/>
<text x="0.9525" y="-0.635" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
<package name="PAD022">
<pad name="1" x="0" y="0" drill="0.5588"/>
<text x="0.9525" y="-0.635" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
<package name="PAD028">
<pad name="1" x="0" y="0" drill="0.7112"/>
<text x="0.9525" y="-0.635" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
<package name="PAD035">
<pad name="1" x="0" y="0" drill="0.889" diameter="1.6002" shape="octagon"/>
<text x="1.27" y="-0.635" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
<package name="PAD040">
<circle x="0" y="0" radius="1.168675" width="0.4064" layer="21"/>
<pad name="1" x="0" y="0" drill="1.0668"/>
<text x="1.5875" y="-0.3175" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
<package name="PAD042">
<pad name="1" x="0" y="0" drill="1.0668" shape="octagon"/>
<text x="1.27" y="-0.635" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
<package name="PAD052">
<pad name="1" x="0" y="0" drill="1.3208" shape="octagon"/>
<text x="1.27" y="-0.635" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
<package name="PAD086">
<pad name="1" x="0" y="0" drill="2.1844" shape="octagon"/>
<text x="1.905" y="-0.635" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
<package name="PAD086-150">
<pad name="1" x="0" y="0" drill="2.1844" diameter="3.81" shape="octagon"/>
<text x="2.2225" y="-0.635" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
<package name="PAD125-200">
<pad name="1" x="0" y="0" drill="3.175" diameter="5.08"/>
<text x="2.8575" y="-0.3175" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
<package name="PAD200">
<pad name="1" x="0" y="0" drill="5.08" diameter="7.62" shape="octagon"/>
<text x="4.1275" y="-0.3175" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
<package name="PAD250">
<pad name="1" x="0" y="0" drill="6.35" diameter="10.16" shape="octagon"/>
<text x="5.715" y="-0.3175" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
<package name="PAD150">
<pad name="1" x="0" y="0" drill="3.81" diameter="6.4516"/>
<text x="3.81" y="-0.635" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="PAD">
<wire x1="-1.016" y1="-1.016" x2="1.016" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="-1.016" y2="1.016" width="0.254" layer="94"/>
<pin name="P" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<text x="1.905" y="-0.635" size="1.4224" layer="95" font="vector">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DO_NOT_INSTALL" prefix="P" uservalue="yes">
<description>NAME:THROUGH HOLE PAD;DESC:DO NOT INSTALL THE PAD -40+150C RoHS;SUP:UNKNOWN;VER:013</description>
<gates>
<gate name="G$1" symbol="PAD" x="0" y="0"/>
</gates>
<devices>
<device name="--125" package="PAD125-250">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-016" package="PAD016">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-022" package="PAD022">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-028" package="PAD028">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-035" package="PAD035">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-040" package="PAD040">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-042" package="PAD042">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-052" package="PAD052">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-086" package="PAD086">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-086-150" package="PAD086-150">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-125" package="PAD125-200">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-200" package="PAD200">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-250" package="PAD250">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-254" package="PAD150">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="EXB-V4V103JV">
<packages>
<package name="EXB-V4V103JV">
<wire x1="-0.9874" y1="1.6701" x2="0.9588" y2="1.6701" width="0.4064" layer="21"/>
<wire x1="0.9588" y1="1.6701" x2="0.9588" y2="-1.6701" width="0.4064" layer="21"/>
<wire x1="0.9588" y1="-1.6701" x2="-0.9874" y2="-1.6701" width="0.4064" layer="21"/>
<wire x1="-0.9874" y1="-1.6701" x2="-0.9874" y2="1.6701" width="0.4064" layer="21"/>
<wire x1="-0.4064" y1="-0.7112" x2="-0.4064" y2="-1.0922" width="0.6096" layer="51"/>
<smd name="1" x="-0.4062" y="-0.9" dx="0.5" dy="0.9" layer="1"/>
<smd name="2" x="0.3938" y="-0.9" dx="0.5" dy="0.9" layer="1"/>
<smd name="3" x="0.3938" y="0.9" dx="0.5" dy="0.9" layer="1"/>
<smd name="4" x="-0.4062" y="0.9" dx="0.5" dy="0.9" layer="1"/>
<text x="1.5875" y="-1.27" size="1.27" layer="27" font="vector" ratio="18">&gt;VALUE</text>
<text x="1.5875" y="0.3175" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="R">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.175" y="3.175" size="1.4224" layer="95" font="vector">&gt;NAME</text>
<text x="-3.81" y="1.27" size="1.4224" layer="96" font="vector">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="EXB-V4V103JV" prefix="RA">
<description>NAME:EXB-V4V103JV;DESC:RES ARRAY 10K OHM 5% 2 RES SMD Chip Resistor 0603x2 -55+125C RoHS;SUP:Digikey Y2103CT-ND;VER:003</description>
<gates>
<gate name=".1" symbol="R" x="0" y="3.81"/>
<gate name=".2" symbol="R" x="0" y="-3.81"/>
</gates>
<devices>
<device name="" package="EXB-V4V103JV">
<connects>
<connect gate=".1" pin="1" pad="1"/>
<connect gate=".1" pin="2" pad="4"/>
<connect gate=".2" pin="1" pad="2"/>
<connect gate=".2" pin="2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="VIRTUAL_JUMPER_ON">
<packages>
<package name="VIRTUAL_JUMPER_ON_TOP">
<wire x1="1.6581" y1="-0.8644" x2="1.6581" y2="0.8643" width="0.4064" layer="21"/>
<wire x1="1.6581" y1="0.8643" x2="-1.6581" y2="0.8643" width="0.4064" layer="21"/>
<wire x1="-1.6581" y1="0.8643" x2="-1.6581" y2="-0.8644" width="0.4064" layer="21"/>
<wire x1="-1.6581" y1="-0.8644" x2="1.6581" y2="-0.8644" width="0.4064" layer="21"/>
<wire x1="-0.9525" y1="0" x2="0.9525" y2="0" width="0.6096" layer="200"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-3.175" y="2.8575" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
<text x="-3.81" y="1.27" size="1.27" layer="27" font="vector" ratio="18">&gt;VALUE</text>
</package>
<package name="VIRTUAL_JUMPER_ON_BOTTOM">
<wire x1="1.6581" y1="-0.8644" x2="1.6581" y2="0.8643" width="0.4064" layer="21"/>
<wire x1="1.6581" y1="0.8643" x2="-1.6581" y2="0.8643" width="0.4064" layer="21"/>
<wire x1="-1.6581" y1="0.8643" x2="-1.6581" y2="-0.8644" width="0.4064" layer="21"/>
<wire x1="-1.6581" y1="-0.8644" x2="1.6581" y2="-0.8644" width="0.4064" layer="21"/>
<wire x1="-0.9525" y1="0" x2="0.9525" y2="0" width="0.6096" layer="201"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-3.175" y="2.8575" size="1.27" layer="25" font="vector" ratio="18">&gt;NAME</text>
<text x="-3.81" y="1.27" size="1.27" layer="27" font="vector" ratio="18">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="VIRTUAL_JUMPER_ON">
<rectangle x1="-2.032" y1="1.27" x2="2.032" y2="1.905" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="0.635" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="0.635" width="0.4064" layer="94"/>
<wire x1="-1.905" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="1.905" y1="0" x2="1.905" y2="0.635" width="0.254" layer="94"/>
<wire x1="1.905" y1="0.635" x2="1.27" y2="0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="0.635" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="-1.905" y2="0.635" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-1.905" y2="0" width="0.254" layer="94"/>
<pin name="1" x="-1.27" y="-1.27" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="2" x="1.27" y="-1.27" visible="off" length="short" direction="pas" rot="R90"/>
<text x="-1.905" y="3.175" size="1.4224" layer="95" font="vector">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DO_NOT_INSTALL" prefix="JP">
<description>NAME:VIRTUAL_JUMPER_ON;DESC:DO NOT INSTALL THE VIRTUAL JUMPER;SUP:UNKNOWN;VER:002</description>
<gates>
<gate name="G$1" symbol="VIRTUAL_JUMPER_ON" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="VIRTUAL_JUMPER_ON_TOP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="&quot;" package="VIRTUAL_JUMPER_ON_BOTTOM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$54" library="frames" deviceset="DINA4_L" device=""/>
<part name="U$22" library="supply2" deviceset="GND" device=""/>
<part name="C3" library="C0603C104K5RACTU" deviceset="C0603C104K5RACTU" device="" value="C0603C104K5RACTU"/>
<part name="U$10" library="supply2" deviceset="GND" device=""/>
<part name="U$17" library="supply2" deviceset="GND" device=""/>
<part name="PORT95" library="01PORT" deviceset="PORT5" device=""/>
<part name="PORT28" library="01PORT" deviceset="PORT5" device=""/>
<part name="U$23" library="supply2" deviceset="GND" device=""/>
<part name="PORT52" library="01PORT" deviceset="PORT5" device=""/>
<part name="U$11" library="supply2" deviceset="GND" device=""/>
<part name="C2" library="TPSC107K010R0100" deviceset="TPSC107K010R0100" device=""/>
<part name="FM1" library="FM" deviceset="DO_NOT_INSTALL_FM" device=""/>
<part name="FM2" library="FM" deviceset="DO_NOT_INSTALL_FM" device=""/>
<part name="FM3" library="FM" deviceset="DO_NOT_INSTALL_FM" device=""/>
<part name="IC1" library="NL-SW-LTE-S7588" deviceset="NL-SW-LTE-S7588" device="-S" value="DO_NOT_INSTALL_NL-SW-LTE-S7588"/>
<part name="X1" library="PCI_EXPRESS_MINI" deviceset="PCI_EXPRESS_MINI" device="" value="DO_NOT_INSTALL_PCI EXPRESS MINI"/>
<part name="U$1" library="supply2" deviceset="GND" device=""/>
<part name="U$4" library="supply2" deviceset="GND" device=""/>
<part name="U$2" library="02_supply2" deviceset="3.3V" device=""/>
<part name="PORT1" library="01PORT" deviceset="PORT5" device=""/>
<part name="PORT2" library="01PORT" deviceset="PORT5" device=""/>
<part name="C4" library="C0603C104K5RACTU" deviceset="C0603C104K5RACTU" device="" value="C0603C104K5RACTU"/>
<part name="D1" library="1N4148W-7-F" deviceset="1N4148W-7-F" device=""/>
<part name="PORT3" library="01PORT" deviceset="PORT5" device=""/>
<part name="R2" library="ERJ-3EKF1001V" deviceset="ERJ-3EKF1001V" device=""/>
<part name="U$5" library="supply2" deviceset="GND" device=""/>
<part name="M1" library="FDV301N" deviceset="FDV301N" device=""/>
<part name="U$3" library="supply2" deviceset="GND" device=""/>
<part name="PORT4" library="01PORT" deviceset="PORT5" device=""/>
<part name="U$6" library="02_supply2" deviceset="3.3V" device=""/>
<part name="PORT5" library="01PORT" deviceset="PORT5" device=""/>
<part name="U$7" library="02_supply2" deviceset="3.3V" device=""/>
<part name="R3" library="ERJ-3EKF1001V" deviceset="ERJ-3EKF1001V" device=""/>
<part name="U$9" library="supply2" deviceset="GND" device=""/>
<part name="M2" library="FDV301N" deviceset="FDV301N" device=""/>
<part name="U$12" library="supply2" deviceset="GND" device=""/>
<part name="PORT6" library="01PORT" deviceset="PORT5" device=""/>
<part name="U$13" library="02_supply2" deviceset="3.3V" device=""/>
<part name="PORT7" library="01PORT" deviceset="PORT5" device=""/>
<part name="U$14" library="02_supply2" deviceset="3.3V" device=""/>
<part name="U$15" library="02_supply2" deviceset="3.3V" device=""/>
<part name="C1" library="TPSC107K010R0100" deviceset="TPSC107K010R0100" device=""/>
<part name="U$16" library="supply2" deviceset="GND" device=""/>
<part name="C5" library="C0603C104K5RACTU" deviceset="C0603C104K5RACTU" device="" value="C0603C104K5RACTU"/>
<part name="U$19" library="supply2" deviceset="GND" device=""/>
<part name="C6" library="C0603C104K5RACTU" deviceset="C0603C104K5RACTU" device="" value="C0603C104K5RACTU"/>
<part name="U$20" library="supply2" deviceset="GND" device=""/>
<part name="PORT9" library="01PORT" deviceset="PORT5" device=""/>
<part name="SOCKET1" library="NPPN101BFLC-RC" deviceset="NPPN101BFLC-RC" device=""/>
<part name="SOCKET2" library="NPPN101BFLC-RC" deviceset="NPPN101BFLC-RC" device=""/>
<part name="IC3" library="TPS61230DRCR" deviceset="TPS61230DRCR" device=""/>
<part name="C7" library="C1608X5R1A226M080AC" deviceset="C1608X5R1A226M080AC" device=""/>
<part name="C8" library="C0603C104K5RACTU" deviceset="C0603C104K5RACTU" device="" value="C0603C104K5RACTU"/>
<part name="U$24" library="supply2" deviceset="GND" device=""/>
<part name="L1" library="SPM5030T-1R0M" deviceset="SPM5030T-1R0M" device=""/>
<part name="R4" library="ERJ-3EKF1003V" deviceset="ERJ-3EKF1003V" device=""/>
<part name="U$25" library="02_supply2" deviceset="3.3V" device=""/>
<part name="PORT8" library="01PORT" deviceset="PORT5" device=""/>
<part name="C9" library="C0603C103K5RACTU" deviceset="C0603C103K5RACTU" device=""/>
<part name="U$26" library="supply2" deviceset="GND" device=""/>
<part name="U$27" library="supply2" deviceset="GND" device=""/>
<part name="R5" library="ERJ-3EKF3003V" deviceset="ERJ-3EKF3003V" device=""/>
<part name="R6" library="ERJ-3EKF1003V" deviceset="ERJ-3EKF1003V" device=""/>
<part name="U$28" library="supply2" deviceset="GND" device=""/>
<part name="C10" library="C1608C0G1H180J" deviceset="C1608C0G1H180J" device=""/>
<part name="C11" library="C1608X5R1A226M080AC" deviceset="C1608X5R1A226M080AC" device=""/>
<part name="U$29" library="supply2" deviceset="GND" device=""/>
<part name="C12" library="C1608X5R1A226M080AC" deviceset="C1608X5R1A226M080AC" device=""/>
<part name="C13" library="C1608X5R1A226M080AC" deviceset="C1608X5R1A226M080AC" device=""/>
<part name="U$30" library="02_supply2" deviceset="4.0V" device=""/>
<part name="U$31" library="02_supply2" deviceset="4.0V" device=""/>
<part name="PORT10" library="01PORT" deviceset="PORT5" device=""/>
<part name="U$8" library="supply2" deviceset="GND" device=""/>
<part name="P1" library="THPAD" deviceset="DO_NOT_INSTALL" device="-016" value="DO_NOT_INSTALL"/>
<part name="P2" library="THPAD" deviceset="DO_NOT_INSTALL" device="-016" value="DO_NOT_INSTALL"/>
<part name="P3" library="THPAD" deviceset="DO_NOT_INSTALL" device="-016" value="DO_NOT_INSTALL"/>
<part name="P4" library="THPAD" deviceset="DO_NOT_INSTALL" device="-016" value="DO_NOT_INSTALL"/>
<part name="P5" library="THPAD" deviceset="DO_NOT_INSTALL" device="-016" value="DO_NOT_INSTALL"/>
<part name="RA1" library="EXB-V4V103JV" deviceset="EXB-V4V103JV" device=""/>
<part name="JP1" library="VIRTUAL_JUMPER_ON" deviceset="DO_NOT_INSTALL" device=""/>
<part name="JP2" library="VIRTUAL_JUMPER_ON" deviceset="DO_NOT_INSTALL" device=""/>
<part name="JP3" library="VIRTUAL_JUMPER_ON" deviceset="DO_NOT_INSTALL" device=""/>
<part name="4.0V" library="THPAD" deviceset="DO_NOT_INSTALL" device="-040" value="DO_NOT_INSTALL-040"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="193.675" y="27.305" size="3.81" layer="94" font="vector">MODEM ADAPTER</text>
<text x="251.46" y="7.62" size="2.54" layer="94" font="vector">1.03</text>
<text x="179.07" y="105.41" size="1.4224" layer="250" font="vector">MODEM</text>
<text x="137.16" y="137.795" size="1.4224" layer="250" font="vector" rot="MR0">0.1uF/50V</text>
<text x="120.015" y="137.795" size="1.4224" layer="250" font="vector" rot="MR0">100uF/10V</text>
<text x="104.775" y="116.205" size="1.4224" layer="250" font="vector" rot="MR0">0.1uF/50V</text>
<text x="154.305" y="103.505" size="1.4224" layer="250" font="vector" rot="MR0">1K 1%</text>
<text x="246.38" y="151.765" size="1.4224" layer="250" font="vector" rot="MR0">10K</text>
<text x="226.695" y="99.06" size="1.4224" layer="250" font="vector">1K 1%</text>
<text x="246.38" y="114.3" size="1.4224" layer="250" font="vector" rot="MR0">10K</text>
<text x="101.6" y="138.43" size="1.4224" layer="250" font="vector" rot="MR0">100uF/10V</text>
<text x="142.24" y="102.87" size="1.4224" layer="250" font="vector" rot="MR0">0.1uF/50V</text>
<text x="217.805" y="98.425" size="1.4224" layer="250" font="vector" rot="MR0">0.1uF/50V</text>
<text x="36.195" y="46.99" size="1.4224" layer="250" font="vector" rot="MR0">22uF/10V</text>
<text x="46.99" y="47.625" size="1.4224" layer="250" font="vector">0.1uF/50V</text>
<text x="74.295" y="46.99" size="1.4224" layer="250" font="vector" rot="MR0">100K 1%</text>
<text x="73.025" y="22.86" size="1.4224" layer="250" font="vector" rot="MR0">10nF/50V</text>
<text x="123.825" y="38.1" size="1.4224" layer="250" font="vector">100K 1%</text>
<text x="120.015" y="52.07" size="1.4224" layer="250" font="vector" rot="MR0">300K 1%</text>
<text x="130.175" y="52.07" size="1.4224" layer="250" font="vector">18pF/50V</text>
<text x="153.67" y="52.07" size="1.4224" layer="250" font="vector" rot="MR0">22uF/10V</text>
<text x="163.195" y="52.07" size="1.4224" layer="250" font="vector">22uF/10V</text>
<text x="178.435" y="52.07" size="1.4224" layer="250" font="vector">22uF/10V</text>
<text x="62.23" y="62.865" size="1.4224" layer="250" font="vector">1.0uH</text>
<text x="172.72" y="137.16" size="1.4224" layer="250" font="vector">NL-SW-LTE-S7588</text>
<text x="31.115" y="168.275" size="1.4224" layer="250" font="vector">PCI EXPRESS MINI</text>
</plain>
<instances>
<instance part="U$54" gate="G$1" x="0" y="0"/>
<instance part="U$54" gate="G$2" x="162.56" y="0"/>
<instance part="FM1" gate="G$1" x="162.56" y="0"/>
<instance part="FM2" gate="G$1" x="162.56" y="0"/>
<instance part="FM3" gate="G$1" x="162.56" y="0"/>
<instance part="U$22" gate="GND" x="165.735" y="106.68" smashed="yes" rot="MR0">
<attribute name="VALUE" x="163.83" y="103.505" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="C3" gate="1" x="139.7" y="139.065" smashed="yes" rot="MR270">
<attribute name="NAME" x="137.16" y="139.7" size="1.4224" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="U$10" gate="GND" x="139.7" y="131.445" smashed="yes" rot="MR0">
<attribute name="VALUE" x="137.795" y="128.27" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="U$17" gate="GND" x="122.555" y="131.445" smashed="yes">
<attribute name="VALUE" x="120.65" y="128.27" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="PORT95" gate="G$1" x="163.195" y="118.11"/>
<instance part="PORT28" gate="G$1" x="97.79" y="123.19"/>
<instance part="U$23" gate="GND" x="206.375" y="106.68" smashed="yes" rot="MR0">
<attribute name="VALUE" x="204.47" y="103.505" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="PORT52" gate="G$1" x="163.195" y="115.57"/>
<instance part="U$11" gate="GND" x="106.68" y="110.49" smashed="yes" rot="MR0">
<attribute name="VALUE" x="104.775" y="107.315" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="C2" gate="G$1" x="122.555" y="139.065" smashed="yes" rot="MR0">
<attribute name="NAME" x="120.015" y="139.7" size="1.4224" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="IC1" gate="G$1" x="182.245" y="121.92" smashed="yes">
<attribute name="NAME" x="180.975" y="139.065" size="1.4224" layer="95" font="vector"/>
</instance>
<instance part="X1" gate="G$1" x="41.275" y="130.81" smashed="yes">
<attribute name="NAME" x="40.64" y="170.18" size="1.4224" layer="95" font="vector"/>
</instance>
<instance part="U$1" gate="GND" x="12.7" y="91.44" smashed="yes" rot="MR0">
<attribute name="VALUE" x="10.795" y="88.265" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="U$4" gate="GND" x="69.215" y="90.17" smashed="yes" rot="MR0">
<attribute name="VALUE" x="67.31" y="86.995" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="U$2" gate="G$1" x="125.095" y="116.84" smashed="yes">
<attribute name="VALUE" x="127" y="121.285" size="1.4224" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="PORT1" gate="G$1" x="74.295" y="119.38" rot="R180"/>
<instance part="PORT2" gate="G$1" x="74.295" y="116.84" rot="R180"/>
<instance part="C4" gate="1" x="106.68" y="118.11" smashed="yes" rot="MR270">
<attribute name="NAME" x="104.14" y="118.745" size="1.4224" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="D1" gate="G$1" x="111.76" y="123.19" smashed="yes" rot="R180">
<attribute name="NAME" x="111.76" y="127.635" size="1.4224" layer="95" font="vector"/>
<attribute name="VALUE" x="106.045" y="125.73" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="PORT3" gate="G$1" x="74.295" y="137.16" rot="R180"/>
<instance part="R2" gate="G$1" x="156.21" y="105.41" smashed="yes" rot="R90">
<attribute name="NAME" x="154.305" y="107.315" size="1.4224" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="U$5" gate="GND" x="156.21" y="96.52" smashed="yes" rot="MR0">
<attribute name="VALUE" x="154.305" y="93.345" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="M1" gate="G$1" x="241.3" y="141.605" smashed="yes">
<attribute name="VALUE" x="235.585" y="146.05" size="1.4224" layer="96" font="vector"/>
<attribute name="NAME" x="239.395" y="147.955" size="1.4224" layer="95" font="vector"/>
</instance>
<instance part="U$3" gate="GND" x="248.285" y="134.62" smashed="yes" rot="MR0">
<attribute name="VALUE" x="246.38" y="131.445" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="PORT4" gate="G$1" x="252.095" y="144.145" rot="R180"/>
<instance part="U$6" gate="G$1" x="248.285" y="162.56" smashed="yes">
<attribute name="VALUE" x="246.38" y="165.735" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="PORT5" gate="G$1" x="14.605" y="165.1"/>
<instance part="U$7" gate="G$1" x="231.775" y="123.825" smashed="yes">
<attribute name="VALUE" x="229.87" y="127" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="R3" gate="G$1" x="224.79" y="100.965" smashed="yes" rot="MR90">
<attribute name="NAME" x="226.695" y="102.87" size="1.4224" layer="95" font="vector" rot="MR180"/>
</instance>
<instance part="U$9" gate="GND" x="224.79" y="92.075" smashed="yes">
<attribute name="VALUE" x="226.695" y="88.9" size="1.4224" layer="96" font="vector" rot="MR0"/>
</instance>
<instance part="M2" gate="G$1" x="241.3" y="104.775" smashed="yes">
<attribute name="VALUE" x="235.585" y="109.22" size="1.4224" layer="96" font="vector"/>
<attribute name="NAME" x="239.395" y="111.76" size="1.4224" layer="95" font="vector"/>
</instance>
<instance part="U$12" gate="GND" x="248.285" y="97.79" smashed="yes" rot="MR0">
<attribute name="VALUE" x="246.38" y="94.615" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="PORT6" gate="G$1" x="252.095" y="107.315" rot="R180"/>
<instance part="U$13" gate="G$1" x="248.285" y="124.46" smashed="yes">
<attribute name="VALUE" x="246.38" y="127.635" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="PORT7" gate="G$1" x="74.295" y="111.76" rot="R180"/>
<instance part="U$14" gate="G$1" x="6.985" y="120.015" smashed="yes">
<attribute name="VALUE" x="5.08" y="123.19" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="U$15" gate="G$1" x="66.675" y="170.815" smashed="yes">
<attribute name="VALUE" x="64.77" y="173.99" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="C1" gate="G$1" x="104.14" y="139.7" smashed="yes" rot="MR0">
<attribute name="NAME" x="101.6" y="140.335" size="1.4224" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="U$16" gate="GND" x="104.14" y="132.08" smashed="yes">
<attribute name="VALUE" x="102.235" y="128.905" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="C5" gate="1" x="144.78" y="104.14" smashed="yes" rot="MR270">
<attribute name="NAME" x="142.24" y="104.775" size="1.4224" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="U$19" gate="GND" x="144.78" y="96.52" smashed="yes" rot="MR0">
<attribute name="VALUE" x="142.875" y="93.345" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="C6" gate="1" x="220.345" y="99.695" smashed="yes" rot="MR270">
<attribute name="NAME" x="217.805" y="100.33" size="1.4224" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="U$20" gate="GND" x="220.345" y="92.075" smashed="yes" rot="MR0">
<attribute name="VALUE" x="218.44" y="88.9" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="PORT9" gate="G$1" x="163.195" y="118.11"/>
<instance part="SOCKET1" gate="SOCKET$1" x="165.1" y="139.7" smashed="yes">
<attribute name="VALUE" x="157.7975" y="141.605" size="1.4224" layer="96" font="vector"/>
<attribute name="NAME" x="160.655" y="143.51" size="1.4224" layer="95" font="vector"/>
</instance>
<instance part="SOCKET2" gate="SOCKET$1" x="198.12" y="139.7" smashed="yes">
<attribute name="VALUE" x="190.8175" y="141.605" size="1.4224" layer="96" font="vector"/>
<attribute name="NAME" x="193.675" y="143.51" size="1.4224" layer="95" font="vector"/>
</instance>
<instance part="IC3" gate="G$1" x="93.98" y="48.26"/>
<instance part="C7" gate="1" x="38.735" y="48.895" smashed="yes" rot="R90">
<attribute name="NAME" x="36.195" y="50.165" size="1.4224" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="C8" gate="1" x="44.45" y="48.895" smashed="yes" rot="R270">
<attribute name="NAME" x="46.99" y="49.53" size="1.4224" layer="95" font="vector"/>
</instance>
<instance part="U$24" gate="GND" x="38.735" y="37.465" smashed="yes">
<attribute name="VALUE" x="40.64" y="34.29" size="1.4224" layer="96" font="vector" rot="MR0"/>
</instance>
<instance part="L1" gate="G$1" x="64.77" y="60.96" smashed="yes">
<attribute name="NAME" x="63.5" y="66.675" size="1.4224" layer="95" font="vector"/>
<attribute name="VALUE" x="56.515" y="64.77" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="R4" gate="G$1" x="76.2" y="48.26" smashed="yes" rot="R90">
<attribute name="NAME" x="74.93" y="50.5714" size="1.4224" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="U$25" gate="G$1" x="38.735" y="65.405" smashed="yes">
<attribute name="VALUE" x="36.83" y="68.58" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="PORT8" gate="G$1" x="66.675" y="40.64"/>
<instance part="C9" gate="1" x="75.565" y="24.13" smashed="yes" rot="R90">
<attribute name="NAME" x="73.025" y="26.035" size="1.4224" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="U$26" gate="GND" x="75.565" y="16.51" smashed="yes">
<attribute name="VALUE" x="77.47" y="13.335" size="1.4224" layer="96" font="vector" rot="MR0"/>
</instance>
<instance part="U$27" gate="GND" x="109.22" y="20.32" smashed="yes">
<attribute name="VALUE" x="111.125" y="17.145" size="1.4224" layer="96" font="vector" rot="MR0"/>
</instance>
<instance part="R5" gate="G$1" x="121.92" y="53.975" smashed="yes" rot="R90">
<attribute name="NAME" x="120.015" y="55.6514" size="1.4224" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="R6" gate="G$1" x="121.92" y="39.37" smashed="yes" rot="MR90">
<attribute name="NAME" x="123.19" y="41.6814" size="1.4224" layer="95" font="vector" rot="MR180"/>
</instance>
<instance part="U$28" gate="GND" x="121.92" y="30.48" smashed="yes">
<attribute name="VALUE" x="123.825" y="27.305" size="1.4224" layer="96" font="vector" rot="MR0"/>
</instance>
<instance part="C10" gate="1" x="127.635" y="53.975" smashed="yes" rot="R270">
<attribute name="NAME" x="130.175" y="54.61" size="1.4224" layer="95" font="vector"/>
</instance>
<instance part="C11" gate="1" x="156.21" y="53.975" smashed="yes" rot="R90">
<attribute name="NAME" x="153.67" y="55.245" size="1.4224" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="U$29" gate="GND" x="156.21" y="43.815" smashed="yes">
<attribute name="VALUE" x="158.115" y="40.64" size="1.4224" layer="96" font="vector" rot="MR0"/>
</instance>
<instance part="C12" gate="1" x="160.655" y="53.975" smashed="yes" rot="MR90">
<attribute name="NAME" x="163.195" y="55.245" size="1.4224" layer="95" font="vector" rot="MR180"/>
</instance>
<instance part="C13" gate="1" x="175.895" y="53.975" smashed="yes" rot="MR90">
<attribute name="NAME" x="178.435" y="55.245" size="1.4224" layer="95" font="vector" rot="MR180"/>
</instance>
<instance part="U$30" gate="G$1" x="175.895" y="65.405"/>
<instance part="U$31" gate="G$1" x="104.14" y="148.59"/>
<instance part="PORT10" gate="G$1" x="74.295" y="139.7" rot="R180"/>
<instance part="U$8" gate="GND" x="211.455" y="129.54" smashed="yes" rot="MR0">
<attribute name="VALUE" x="209.55" y="126.365" size="1.4224" layer="96" font="vector"/>
</instance>
<instance part="P1" gate="G$1" x="163.195" y="130.81" rot="R180"/>
<instance part="P2" gate="G$1" x="163.195" y="128.27" rot="R180"/>
<instance part="P3" gate="G$1" x="201.295" y="130.81"/>
<instance part="P4" gate="G$1" x="201.295" y="128.27"/>
<instance part="P5" gate="G$1" x="201.295" y="113.03"/>
<instance part="RA1" gate=".1" x="248.285" y="153.67" smashed="yes" rot="R90">
<attribute name="NAME" x="246.38" y="154.94" size="1.4224" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="RA1" gate=".2" x="248.285" y="115.57" smashed="yes" rot="R90">
<attribute name="NAME" x="246.38" y="117.475" size="1.4224" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="JP1" gate="G$1" x="133.985" y="125.095"/>
<instance part="JP2" gate="G$1" x="133.985" y="114.935"/>
<instance part="JP3" gate="G$1" x="203.835" y="135.255"/>
<instance part="4.0V" gate="G$1" x="181.61" y="60.96" smashed="yes">
<attribute name="NAME" x="183.515" y="61.595" size="1.4224" layer="95" font="vector" rot="MR180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C3" gate="1" pin="2"/>
<pinref part="U$10" gate="GND" pin="GND"/>
<wire x1="139.7" y1="133.985" x2="139.7" y2="135.255" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$17" gate="GND" pin="GND"/>
<wire x1="122.555" y1="133.985" x2="122.555" y2="135.255" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="-"/>
</segment>
<segment>
<pinref part="U$23" gate="GND" pin="GND"/>
<wire x1="206.375" y1="120.65" x2="206.375" y2="110.49" width="0.1524" layer="91"/>
<wire x1="206.375" y1="110.49" x2="206.375" y2="109.22" width="0.1524" layer="91"/>
<wire x1="197.485" y1="120.65" x2="206.375" y2="120.65" width="0.1524" layer="91"/>
<wire x1="197.485" y1="110.49" x2="206.375" y2="110.49" width="0.1524" layer="91"/>
<junction x="206.375" y="110.49"/>
<pinref part="IC1" gate="G$1" pin="GND@1"/>
<pinref part="IC1" gate="G$1" pin="GND@2"/>
</segment>
<segment>
<wire x1="167.005" y1="110.49" x2="165.735" y2="110.49" width="0.1524" layer="91"/>
<wire x1="165.735" y1="110.49" x2="165.735" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U$22" gate="GND" pin="GND"/>
<wire x1="167.005" y1="125.73" x2="165.735" y2="125.73" width="0.1524" layer="91"/>
<wire x1="165.735" y1="125.73" x2="165.735" y2="110.49" width="0.1524" layer="91"/>
<junction x="165.735" y="110.49"/>
<pinref part="IC1" gate="G$1" pin="GND"/>
<pinref part="IC1" gate="G$1" pin="GND@3"/>
</segment>
<segment>
<pinref part="U$11" gate="GND" pin="GND"/>
<wire x1="106.68" y1="113.03" x2="106.68" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C4" gate="1" pin="2"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="GND@8"/>
<pinref part="U$1" gate="GND" pin="GND"/>
<wire x1="18.415" y1="109.22" x2="12.7" y2="109.22" width="0.1524" layer="91"/>
<wire x1="12.7" y1="109.22" x2="12.7" y2="96.52" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="GND@6"/>
<wire x1="12.7" y1="96.52" x2="12.7" y2="93.98" width="0.1524" layer="91"/>
<wire x1="18.415" y1="119.38" x2="12.7" y2="119.38" width="0.1524" layer="91"/>
<wire x1="12.7" y1="119.38" x2="12.7" y2="116.84" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="GND@7"/>
<wire x1="12.7" y1="116.84" x2="12.7" y2="109.22" width="0.1524" layer="91"/>
<wire x1="18.415" y1="116.84" x2="12.7" y2="116.84" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="GND@4"/>
<wire x1="18.415" y1="129.54" x2="12.7" y2="129.54" width="0.1524" layer="91"/>
<wire x1="12.7" y1="129.54" x2="12.7" y2="127" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="GND@5"/>
<wire x1="12.7" y1="127" x2="12.7" y2="119.38" width="0.1524" layer="91"/>
<wire x1="18.415" y1="127" x2="12.7" y2="127" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="GND@3"/>
<wire x1="18.415" y1="137.16" x2="12.7" y2="137.16" width="0.1524" layer="91"/>
<wire x1="12.7" y1="137.16" x2="12.7" y2="129.54" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="GND@2"/>
<wire x1="18.415" y1="147.32" x2="12.7" y2="147.32" width="0.1524" layer="91"/>
<wire x1="12.7" y1="147.32" x2="12.7" y2="137.16" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="GND"/>
<wire x1="18.415" y1="154.94" x2="12.7" y2="154.94" width="0.1524" layer="91"/>
<wire x1="12.7" y1="154.94" x2="12.7" y2="147.32" width="0.1524" layer="91"/>
<junction x="12.7" y="147.32"/>
<junction x="12.7" y="137.16"/>
<junction x="12.7" y="129.54"/>
<junction x="12.7" y="127"/>
<junction x="12.7" y="119.38"/>
<junction x="12.7" y="116.84"/>
<junction x="12.7" y="109.22"/>
<pinref part="X1" gate="G$1" pin="PE"/>
<wire x1="18.415" y1="96.52" x2="12.7" y2="96.52" width="0.1524" layer="91"/>
<junction x="12.7" y="96.52"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="GND@1"/>
<pinref part="U$4" gate="GND" pin="GND"/>
<wire x1="64.135" y1="162.56" x2="69.215" y2="162.56" width="0.1524" layer="91"/>
<wire x1="69.215" y1="162.56" x2="69.215" y2="142.24" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="GND@13"/>
<wire x1="69.215" y1="142.24" x2="69.215" y2="132.08" width="0.1524" layer="91"/>
<wire x1="69.215" y1="132.08" x2="69.215" y2="121.92" width="0.1524" layer="91"/>
<wire x1="69.215" y1="121.92" x2="69.215" y2="114.3" width="0.1524" layer="91"/>
<wire x1="69.215" y1="114.3" x2="69.215" y2="101.6" width="0.1524" layer="91"/>
<wire x1="69.215" y1="101.6" x2="69.215" y2="96.52" width="0.1524" layer="91"/>
<wire x1="69.215" y1="95.885" x2="69.215" y2="92.71" width="0.1524" layer="91"/>
<wire x1="64.135" y1="101.6" x2="69.215" y2="101.6" width="0.1524" layer="91"/>
<wire x1="64.135" y1="114.3" x2="69.215" y2="114.3" width="0.1524" layer="91"/>
<wire x1="64.135" y1="132.08" x2="69.215" y2="132.08" width="0.1524" layer="91"/>
<wire x1="64.135" y1="142.24" x2="69.215" y2="142.24" width="0.1524" layer="91"/>
<wire x1="64.135" y1="121.92" x2="69.215" y2="121.92" width="0.1524" layer="91"/>
<junction x="69.215" y="114.3"/>
<junction x="69.215" y="101.6"/>
<junction x="69.215" y="121.92"/>
<junction x="69.215" y="132.08"/>
<junction x="69.215" y="142.24"/>
<pinref part="X1" gate="G$1" pin="GND@9"/>
<pinref part="X1" gate="G$1" pin="GND@10"/>
<pinref part="X1" gate="G$1" pin="GND@11"/>
<pinref part="X1" gate="G$1" pin="GND@12"/>
<pinref part="X1" gate="G$1" pin="PE@1"/>
<wire x1="64.135" y1="96.52" x2="69.215" y2="96.52" width="0.1524" layer="91"/>
<wire x1="69.215" y1="96.52" x2="69.215" y2="95.885" width="0.1524" layer="91"/>
<junction x="69.215" y="96.52"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="U$5" gate="GND" pin="GND"/>
<wire x1="156.21" y1="99.06" x2="156.21" y2="100.33" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="S-N"/>
<pinref part="U$3" gate="GND" pin="GND"/>
<wire x1="245.11" y1="139.065" x2="248.285" y2="139.065" width="0.1524" layer="91"/>
<wire x1="248.285" y1="139.065" x2="248.285" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="U$9" gate="GND" pin="GND"/>
<wire x1="224.79" y1="94.615" x2="224.79" y2="95.885" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="M2" gate="G$1" pin="S-N"/>
<pinref part="U$12" gate="GND" pin="GND"/>
<wire x1="245.11" y1="102.235" x2="248.285" y2="102.235" width="0.1524" layer="91"/>
<wire x1="248.285" y1="102.235" x2="248.285" y2="100.33" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="-"/>
<pinref part="U$16" gate="GND" pin="GND"/>
<wire x1="104.14" y1="135.89" x2="104.14" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C5" gate="1" pin="2"/>
<pinref part="U$19" gate="GND" pin="GND"/>
<wire x1="144.78" y1="99.06" x2="144.78" y2="100.33" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="1" pin="2"/>
<pinref part="U$20" gate="GND" pin="GND"/>
<wire x1="220.345" y1="94.615" x2="220.345" y2="95.885" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C8" gate="1" pin="2"/>
<pinref part="U$24" gate="GND" pin="GND"/>
<wire x1="38.735" y1="40.005" x2="38.735" y2="42.545" width="0.1524" layer="91"/>
<wire x1="38.735" y1="42.545" x2="44.45" y2="42.545" width="0.1524" layer="91"/>
<wire x1="44.45" y1="42.545" x2="44.45" y2="45.085" width="0.1524" layer="91"/>
<pinref part="C7" gate="1" pin="1"/>
<wire x1="38.735" y1="45.085" x2="38.735" y2="42.545" width="0.1524" layer="91"/>
<junction x="38.735" y="42.545"/>
</segment>
<segment>
<pinref part="C9" gate="1" pin="1"/>
<pinref part="U$26" gate="GND" pin="GND"/>
<wire x1="75.565" y1="19.05" x2="75.565" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="PAD"/>
<pinref part="U$27" gate="GND" pin="GND"/>
<wire x1="107.95" y1="30.48" x2="109.22" y2="30.48" width="0.1524" layer="91"/>
<wire x1="109.22" y1="30.48" x2="109.22" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="U$28" gate="GND" pin="GND"/>
<wire x1="121.92" y1="33.02" x2="121.92" y2="34.29" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$29" gate="GND" pin="GND"/>
<pinref part="C11" gate="1" pin="1"/>
<wire x1="156.21" y1="50.165" x2="156.21" y2="48.26" width="0.1524" layer="91"/>
<pinref part="C13" gate="1" pin="1"/>
<wire x1="156.21" y1="48.26" x2="156.21" y2="46.355" width="0.1524" layer="91"/>
<wire x1="156.21" y1="48.26" x2="160.655" y2="48.26" width="0.1524" layer="91"/>
<wire x1="160.655" y1="48.26" x2="175.895" y2="48.26" width="0.1524" layer="91"/>
<wire x1="175.895" y1="48.26" x2="175.895" y2="50.165" width="0.1524" layer="91"/>
<pinref part="C12" gate="1" pin="1"/>
<wire x1="160.655" y1="50.165" x2="160.655" y2="48.26" width="0.1524" layer="91"/>
<junction x="156.21" y="48.26"/>
<junction x="160.655" y="48.26"/>
</segment>
<segment>
<pinref part="U$8" gate="GND" pin="GND"/>
<wire x1="205.105" y1="133.985" x2="205.105" y2="133.35" width="0.1524" layer="91"/>
<wire x1="205.105" y1="133.35" x2="211.455" y2="133.35" width="0.1524" layer="91"/>
<wire x1="211.455" y1="133.35" x2="211.455" y2="132.08" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="SHDN" class="0">
<segment>
<label x="88.9" y="123.825" size="1.4224" layer="91" font="vector" rot="MR180"/>
<pinref part="C4" gate="1" pin="1"/>
<wire x1="106.68" y1="121.92" x2="106.68" y2="123.19" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="109.22" y1="123.19" x2="106.68" y2="123.19" width="0.1524" layer="91"/>
<wire x1="106.68" y1="123.19" x2="99.06" y2="123.19" width="0.1524" layer="91"/>
<junction x="106.68" y="123.19"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="PERST#"/>
<wire x1="64.135" y1="137.16" x2="73.025" y2="137.16" width="0.1524" layer="91"/>
<label x="75.565" y="136.525" size="1.4224" layer="91" font="vector"/>
</segment>
</net>
<net name="IO3" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="DIO5"/>
<wire x1="197.485" y1="125.73" x2="224.79" y2="125.73" width="0.1524" layer="91"/>
<wire x1="224.79" y1="125.73" x2="224.79" y2="139.065" width="0.1524" layer="91"/>
<wire x1="224.79" y1="139.065" x2="234.95" y2="139.065" width="0.1524" layer="91"/>
<pinref part="M1" gate="G$1" pin="G-N"/>
</segment>
</net>
<net name="USB_N" class="0">
<segment>
<wire x1="167.005" y1="115.57" x2="164.465" y2="115.57" width="0.1524" layer="91"/>
<label x="154.305" y="114.935" size="1.4224" layer="91" font="vector"/>
<pinref part="IC1" gate="G$1" pin="USB_D-"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="USB_D-"/>
<wire x1="64.135" y1="119.38" x2="73.025" y2="119.38" width="0.1524" layer="91"/>
<label x="75.565" y="118.745" size="1.4224" layer="91" font="vector"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<wire x1="132.715" y1="113.665" x2="132.715" y2="112.395" width="0.1524" layer="91"/>
<wire x1="132.715" y1="112.395" x2="125.095" y2="112.395" width="0.1524" layer="91"/>
<wire x1="125.095" y1="112.395" x2="125.095" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="3.3V"/>
<pinref part="JP2" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="3.3V"/>
<wire x1="248.285" y1="160.02" x2="248.285" y2="158.75" width="0.1524" layer="91"/>
<pinref part="RA1" gate=".1" pin="2"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VREF"/>
<pinref part="U$7" gate="G$1" pin="3.3V"/>
<wire x1="231.775" y1="121.285" x2="231.775" y2="118.11" width="0.1524" layer="91"/>
<wire x1="231.775" y1="118.11" x2="220.345" y2="118.11" width="0.1524" layer="91"/>
<pinref part="C6" gate="1" pin="1"/>
<wire x1="220.345" y1="118.11" x2="197.485" y2="118.11" width="0.1524" layer="91"/>
<wire x1="220.345" y1="103.505" x2="220.345" y2="118.11" width="0.1524" layer="91"/>
<junction x="220.345" y="118.11"/>
</segment>
<segment>
<pinref part="U$13" gate="G$1" pin="3.3V"/>
<wire x1="248.285" y1="121.92" x2="248.285" y2="120.65" width="0.1524" layer="91"/>
<pinref part="RA1" gate=".2" pin="2"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="+3.3VAUX@1"/>
<pinref part="U$14" gate="G$1" pin="3.3V"/>
<wire x1="18.415" y1="114.3" x2="6.985" y2="114.3" width="0.1524" layer="91"/>
<wire x1="6.985" y1="114.3" x2="6.985" y2="117.475" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="+3.3VAUX@2"/>
<wire x1="18.415" y1="111.76" x2="6.985" y2="111.76" width="0.1524" layer="91"/>
<wire x1="6.985" y1="111.76" x2="6.985" y2="114.3" width="0.1524" layer="91"/>
<junction x="6.985" y="114.3"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="3.3VAUX"/>
<wire x1="64.135" y1="165.1" x2="66.675" y2="165.1" width="0.1524" layer="91"/>
<wire x1="66.675" y1="165.1" x2="66.675" y2="168.275" width="0.1524" layer="91"/>
<pinref part="U$15" gate="G$1" pin="3.3V"/>
<pinref part="X1" gate="G$1" pin="+3.3VAUX@4"/>
<wire x1="64.135" y1="99.06" x2="66.675" y2="99.06" width="0.1524" layer="91"/>
<wire x1="66.675" y1="99.06" x2="66.675" y2="134.62" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="+3.3VAUX@3"/>
<wire x1="66.675" y1="134.62" x2="66.675" y2="165.1" width="0.1524" layer="91"/>
<wire x1="64.135" y1="134.62" x2="66.675" y2="134.62" width="0.1524" layer="91"/>
<junction x="66.675" y="134.62"/>
<junction x="66.675" y="165.1"/>
</segment>
<segment>
<pinref part="C7" gate="1" pin="2"/>
<wire x1="38.735" y1="52.705" x2="38.735" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C8" gate="1" pin="1"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="58.42" y1="60.96" x2="52.07" y2="60.96" width="0.1524" layer="91"/>
<wire x1="52.07" y1="60.96" x2="44.45" y2="60.96" width="0.1524" layer="91"/>
<wire x1="44.45" y1="60.96" x2="44.45" y2="52.705" width="0.1524" layer="91"/>
<wire x1="38.735" y1="60.96" x2="44.45" y2="60.96" width="0.1524" layer="91"/>
<wire x1="80.01" y1="54.61" x2="76.2" y2="54.61" width="0.1524" layer="91"/>
<wire x1="76.2" y1="54.61" x2="52.07" y2="54.61" width="0.1524" layer="91"/>
<wire x1="52.07" y1="54.61" x2="52.07" y2="60.96" width="0.1524" layer="91"/>
<junction x="38.735" y="60.96"/>
<junction x="44.45" y="60.96"/>
<junction x="52.07" y="60.96"/>
<pinref part="IC3" gate="G$1" pin="VIN"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="76.2" y1="53.34" x2="76.2" y2="54.61" width="0.1524" layer="91"/>
<junction x="76.2" y="54.61"/>
<pinref part="U$25" gate="G$1" pin="3.3V"/>
<wire x1="38.735" y1="62.865" x2="38.735" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VUSB"/>
<wire x1="135.255" y1="112.395" x2="144.78" y2="112.395" width="0.1524" layer="91"/>
<wire x1="144.78" y1="112.395" x2="144.78" y2="120.65" width="0.1524" layer="91"/>
<pinref part="C5" gate="1" pin="1"/>
<wire x1="144.78" y1="120.65" x2="167.005" y2="120.65" width="0.1524" layer="91"/>
<wire x1="144.78" y1="107.95" x2="144.78" y2="112.395" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="2"/>
<wire x1="135.255" y1="112.395" x2="135.255" y2="113.665" width="0.1524" layer="91"/>
<junction x="144.78" y="112.395"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="DTR(I)"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="167.005" y1="113.03" x2="156.21" y2="113.03" width="0.1524" layer="91"/>
<wire x1="156.21" y1="113.03" x2="156.21" y2="110.49" width="0.1524" layer="91"/>
</segment>
</net>
<net name="WAKE" class="0">
<segment>
<pinref part="M1" gate="G$1" pin="D-N"/>
<wire x1="245.11" y1="144.145" x2="248.285" y2="144.145" width="0.1524" layer="91"/>
<wire x1="248.285" y1="148.59" x2="248.285" y2="144.145" width="0.1524" layer="91"/>
<wire x1="248.285" y1="144.145" x2="250.825" y2="144.145" width="0.1524" layer="91"/>
<junction x="248.285" y="144.145"/>
<label x="253.365" y="143.51" size="1.4224" layer="91" font="vector"/>
<pinref part="RA1" gate=".1" pin="1"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="WAKE#"/>
<wire x1="18.415" y1="165.1" x2="15.875" y2="165.1" width="0.1524" layer="91"/>
<label x="6.35" y="164.465" size="1.4224" layer="91" font="vector"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="RTS(I)"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="197.485" y1="123.19" x2="224.79" y2="123.19" width="0.1524" layer="91"/>
<wire x1="224.79" y1="123.19" x2="224.79" y2="106.045" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VGPIO"/>
<wire x1="197.485" y1="115.57" x2="231.775" y2="115.57" width="0.1524" layer="91"/>
<wire x1="231.775" y1="115.57" x2="231.775" y2="102.235" width="0.1524" layer="91"/>
<wire x1="231.775" y1="102.235" x2="234.95" y2="102.235" width="0.1524" layer="91"/>
<pinref part="M2" gate="G$1" pin="G-N"/>
</segment>
</net>
<net name="LED" class="0">
<segment>
<pinref part="M2" gate="G$1" pin="D-N"/>
<wire x1="245.11" y1="107.315" x2="248.285" y2="107.315" width="0.1524" layer="91"/>
<wire x1="248.285" y1="110.49" x2="248.285" y2="107.315" width="0.1524" layer="91"/>
<wire x1="248.285" y1="107.315" x2="250.825" y2="107.315" width="0.1524" layer="91"/>
<junction x="248.285" y="107.315"/>
<label x="253.365" y="106.68" size="1.4224" layer="91" font="vector"/>
<pinref part="RA1" gate=".2" pin="1"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="LED_WWAN#"/>
<wire x1="64.135" y1="111.76" x2="73.025" y2="111.76" width="0.1524" layer="91"/>
<label x="75.565" y="111.125" size="1.4224" layer="91" font="vector"/>
</segment>
</net>
<net name="USB_P" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="USB_D+"/>
<wire x1="64.135" y1="116.84" x2="73.025" y2="116.84" width="0.1524" layer="91"/>
<label x="75.565" y="116.205" size="1.4224" layer="91" font="vector"/>
</segment>
<segment>
<wire x1="167.005" y1="118.11" x2="164.465" y2="118.11" width="0.1524" layer="91"/>
<label x="154.305" y="117.475" size="1.4224" layer="91" font="vector"/>
<pinref part="IC1" gate="G$1" pin="USB_D+"/>
</segment>
</net>
<net name="EN_4V" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="W_DISABLE#"/>
<wire x1="64.135" y1="139.7" x2="73.025" y2="139.7" width="0.1524" layer="91"/>
<label x="74.93" y="139.065" size="1.4224" layer="91" font="vector"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="EN"/>
<wire x1="80.01" y1="40.64" x2="76.2" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="76.2" y1="40.64" x2="67.945" y2="40.64" width="0.1524" layer="91"/>
<wire x1="76.2" y1="43.18" x2="76.2" y2="40.64" width="0.1524" layer="91"/>
<junction x="76.2" y="40.64"/>
<label x="58.42" y="40.005" size="1.4224" layer="91" font="vector"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="SW@1"/>
<wire x1="76.2" y1="60.96" x2="76.2" y2="58.42" width="0.1524" layer="91"/>
<wire x1="76.2" y1="58.42" x2="80.01" y2="58.42" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="SW"/>
<wire x1="80.01" y1="60.96" x2="76.2" y2="60.96" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="76.2" y1="60.96" x2="71.12" y2="60.96" width="0.1524" layer="91"/>
<junction x="76.2" y="60.96"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="SS"/>
<pinref part="C9" gate="1" pin="2"/>
<wire x1="80.01" y1="30.48" x2="75.565" y2="30.48" width="0.1524" layer="91"/>
<wire x1="75.565" y1="30.48" x2="75.565" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FB" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="FB"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="107.95" y1="45.72" x2="121.92" y2="45.72" width="0.1524" layer="91"/>
<wire x1="121.92" y1="45.72" x2="121.92" y2="48.895" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="121.92" y1="45.72" x2="121.92" y2="44.45" width="0.1524" layer="91"/>
<junction x="121.92" y="45.72"/>
<pinref part="C10" gate="1" pin="2"/>
<wire x1="121.92" y1="45.72" x2="127.635" y2="45.72" width="0.1524" layer="91"/>
<wire x1="127.635" y1="45.72" x2="127.635" y2="50.165" width="0.1524" layer="91"/>
</segment>
</net>
<net name="4.0V" class="0">
<segment>
<wire x1="121.92" y1="60.96" x2="127.635" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C10" gate="1" pin="1"/>
<wire x1="127.635" y1="57.785" x2="127.635" y2="60.96" width="0.1524" layer="91"/>
<junction x="127.635" y="60.96"/>
<pinref part="C13" gate="1" pin="2"/>
<wire x1="127.635" y1="60.96" x2="156.21" y2="60.96" width="0.1524" layer="91"/>
<wire x1="156.21" y1="60.96" x2="160.655" y2="60.96" width="0.1524" layer="91"/>
<wire x1="160.655" y1="60.96" x2="175.895" y2="60.96" width="0.1524" layer="91"/>
<wire x1="175.895" y1="60.96" x2="175.895" y2="57.785" width="0.1524" layer="91"/>
<pinref part="C12" gate="1" pin="2"/>
<wire x1="160.655" y1="57.785" x2="160.655" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C11" gate="1" pin="2"/>
<wire x1="156.21" y1="57.785" x2="156.21" y2="60.96" width="0.1524" layer="91"/>
<junction x="160.655" y="60.96"/>
<junction x="156.21" y="60.96"/>
<junction x="175.895" y="60.96"/>
<pinref part="U$30" gate="G$1" pin="4.0V"/>
<wire x1="175.895" y1="62.865" x2="175.895" y2="60.96" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="VOUT"/>
<wire x1="111.76" y1="60.96" x2="107.95" y2="60.96" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="VOUT@1"/>
<wire x1="107.95" y1="58.42" x2="111.76" y2="58.42" width="0.1524" layer="91"/>
<wire x1="111.76" y1="58.42" x2="111.76" y2="60.96" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="111.76" y1="60.96" x2="121.92" y2="60.96" width="0.1524" layer="91"/>
<wire x1="121.92" y1="60.96" x2="121.92" y2="59.055" width="0.1524" layer="91"/>
<junction x="111.76" y="60.96"/>
<junction x="121.92" y="60.96"/>
<pinref part="4.0V" gate="G$1" pin="P"/>
<wire x1="179.07" y1="60.96" x2="175.895" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="139.7" y1="144.145" x2="156.845" y2="144.145" width="0.1524" layer="91"/>
<wire x1="156.845" y1="144.145" x2="156.845" y2="133.35" width="0.1524" layer="91"/>
<wire x1="156.845" y1="133.35" x2="167.005" y2="133.35" width="0.1524" layer="91"/>
<pinref part="C3" gate="1" pin="1"/>
<wire x1="139.7" y1="142.875" x2="139.7" y2="144.145" width="0.1524" layer="91"/>
<junction x="139.7" y="144.145"/>
<wire x1="139.7" y1="144.145" x2="122.555" y2="144.145" width="0.1524" layer="91"/>
<wire x1="122.555" y1="144.145" x2="122.555" y2="142.875" width="0.1524" layer="91"/>
<junction x="122.555" y="144.145"/>
<pinref part="C2" gate="G$1" pin="+"/>
<pinref part="IC1" gate="G$1" pin="VCC"/>
<pinref part="C1" gate="G$1" pin="+"/>
<wire x1="122.555" y1="144.145" x2="104.14" y2="144.145" width="0.1524" layer="91"/>
<wire x1="104.14" y1="144.145" x2="104.14" y2="143.51" width="0.1524" layer="91"/>
<junction x="104.14" y="144.145"/>
<wire x1="104.14" y1="144.145" x2="104.14" y2="146.05" width="0.1524" layer="91"/>
<pinref part="U$31" gate="G$1" pin="4.0V"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PWR_ON"/>
<wire x1="202.565" y1="133.985" x2="202.565" y2="133.35" width="0.1524" layer="91"/>
<wire x1="202.565" y1="133.35" x2="197.485" y2="133.35" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="132.715" y1="123.825" x2="132.715" y2="123.19" width="0.1524" layer="91"/>
<wire x1="132.715" y1="123.19" x2="114.3" y2="123.19" width="0.1524" layer="91"/>
<pinref part="JP1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="RESET"/>
<wire x1="135.255" y1="123.825" x2="135.255" y2="123.19" width="0.1524" layer="91"/>
<wire x1="135.255" y1="123.19" x2="167.005" y2="123.19" width="0.1524" layer="91"/>
<pinref part="JP1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="DOUT"/>
<pinref part="P1" gate="G$1" pin="P"/>
<wire x1="165.735" y1="130.81" x2="167.005" y2="130.81" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="DIN"/>
<pinref part="P2" gate="G$1" pin="P"/>
<wire x1="165.735" y1="128.27" x2="167.005" y2="128.27" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="ADC1"/>
<pinref part="P3" gate="G$1" pin="P"/>
<wire x1="198.755" y1="130.81" x2="197.485" y2="130.81" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="DIO7"/>
<pinref part="P4" gate="G$1" pin="P"/>
<wire x1="198.755" y1="128.27" x2="197.485" y2="128.27" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="CTS(O)"/>
<pinref part="P5" gate="G$1" pin="P"/>
<wire x1="198.755" y1="113.03" x2="197.485" y2="113.03" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
